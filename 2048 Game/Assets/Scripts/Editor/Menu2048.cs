﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class Menu2048 : MonoBehaviour {

	[MenuItem("2048/Prepare for IOS Build")]
	public static void IOSBuild () {
		UnityEditor.PlayerSettings.applicationIdentifier = "com.beetheswarm.2048";
	}


	[MenuItem("2048/Prepare for Android Build")]
	public static void AndroidBuild () {
		UnityEditor.PlayerSettings.applicationIdentifier = "com.beetheswarm.the2048game";

	}
}
