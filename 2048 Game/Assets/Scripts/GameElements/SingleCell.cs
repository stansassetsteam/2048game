﻿using UnityEngine;
using System.Collections;
using System;

public class SingleCell : MonoBehaviour, IComparable<SingleCell> {
	public int CellIndex = 0;
	public FilledCell CellNod = null;

	[HideInInspector]
	public FilledCell CellForMege = null;

	void Awake() {
		GameController.Instance.AddCellTolist (this);
	}

	public void CreateNodIn() {
		CellNod = ResourcesManager.LoadNod ();

		CellNod.transform.SetParent (this.transform);
		CellNod.transform.localScale = Vector3.one;
		CellNod.transform.localPosition = Vector3.zero;

		CellNod.GetComponent<RectTransform> ().offsetMin = Vector2.zero;
		CellNod.GetComponent<RectTransform> ().offsetMax = Vector2.zero;
	}

	public int CompareTo(SingleCell cel) {
		if (this.CellIndex > cel.CellIndex) {
			return 1;
		} else if (this.CellIndex < cel.CellIndex) {
			return -1;
		} else {
			return 0;
		}
	}
}
