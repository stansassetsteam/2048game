﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FilledCell : MonoBehaviour {
	public int NumberInside = 0;
	public UnityEngine.UI.Image CellColor;
	public UnityEngine.UI.Text NumberTextField;

	private SingleCell _CellToMerdge;

	public static event System.Action GoalReached = delegate {};

	void Start() {
		transform.localScale = Vector3.one * 0.7f;
		iTween.ScaleTo (gameObject, iTween.Hash("scale", Vector3.one, "easeType", iTween.EaseType.easeOutBack, "time", 0.3f));
	}

	public void SetNumber(int newNumber) {
		NumberInside = newNumber;

		if (newNumber == ScoreController.GetCurrentGoalValue()) {
			ScoreController.IncreaseGoalCount ();
			GoalReached ();

			SoundController.Instance.PlaySound (Sounds.win2048);
		}

		CellColor.color = GameController.Instance.ColorDictionary[NumberInside];

		if (newNumber == 4 || newNumber == 512 || newNumber == 1024) {
			NumberTextField.color = Color.black;
		} else {
			NumberTextField.color = Color.white;
		}

		NumberTextField.text = NumberInside.ToString ();
	}

	public void MoveToParrent() {
		MoveTo (transform.parent);
	}

	public void MoveAndMerge(SingleCell cellToMerdge) {
		_CellToMerdge = cellToMerdge;

		MoveTo (_CellToMerdge.transform, true);
		Invoke ("Merge",  GameConfig.MoveAnimationTime - 0.1f);
	}

	public void AnimateMerge() {
		transform.localScale = Vector3.one * 1.2f;
		iTween.ScaleTo (gameObject, iTween.Hash("scale", Vector3.one, "easeType", iTween.EaseType.easeOutBack, "time", 0.3f));
	}

	private void MoveTo(Transform target, bool isMerge = false) {
		if (Vector3.Distance (gameObject.transform.position, target.position) > GameConfig.AllowableMoveError) {

			GameController.Instance.WasAtLeastOneAction = true;

			if (isMerge) {
				SoundController.Instance.PlaySound (Sounds.merge);
			} else {
				SoundController.Instance.PlaySound (Sounds.swipe);
			}

			iTween.MoveTo (gameObject, target.position, GameConfig.MoveAnimationTime);
		}
	}

	private void Merge() {
		int NewNumber = _CellToMerdge.CellNod.NumberInside * 2;
		_CellToMerdge.CellNod.SetNumber (NewNumber);
		_CellToMerdge.CellNod.AnimateMerge ();

		ScoreController.AddToCurrentPoints(NewNumber);

		Destroy (gameObject);
		_CellToMerdge.CellForMege = null;
		_CellToMerdge = null;



	}
}
