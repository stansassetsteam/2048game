﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BTS;

public class LandingSceneController : MonoBehaviour {


	void Awake () {
		//PlayerPrefs.DeleteAll ();

		StartCoroutine (StartLoadLevelAfter ("Portrait", 2.5f));
        BTSPlugin.Init("051928341be67dcba03f0e04104d9047", () => {

        });
    }
	
	IEnumerator StartLoadLevelAfter (string levelName, float delay) {
		yield return new WaitForSeconds (delay);

		Application.LoadLevel (levelName);
	}
}
