﻿using BTS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MultipleChestCounter : Counter {

    [SerializeField] private Image holder;
       
    void Start()
    {
        BTSPlugin.OnUserLoggedOut += OnDisableCounter;
        BTSPlugin.OnUserLoggedIn += OnEnableCounter;
        
        UIPanelController.CheckChestCount += OnCheckChestCount;
        base.Start();
        
        PlayerProgress.Instance.Player.OnChestsSetValueChanged += OnValueChanged;
        OnValueChanged(PlayerProgress.Instance.Player.ChestSet);
    }

    private void OnDestroy()
    {
        if (PlayerProgress.Instance != null) {
            PlayerProgress.Instance.Player.OnChestsSetValueChanged -= OnValueChanged;
        }
    }

    protected override void OnValueChanged(int value)
    {
        if (value > 0)
        {
            holder.enabled = true;
            this.value.gameObject.SetActive(true);
            this.value.text = value.ToString();
        }
        else {
            holder.enabled = false;
            gameObject.SetActive(false);
        }
    }

    public void OnCheckChestCount() {
        BTSPlugin.GetChest(count => {
            PlayerProgress.Instance.Player.Chests = count;
        });

        PlayerProgress.Instance.Player.OnChestsSetValueChanged += OnValueChanged;
        OnValueChanged(PlayerProgress.Instance.Player.ChestSet);
        
    }

    public void OnDisableCounter() {
        
        holder.enabled = false;
        gameObject.SetActive(false);
        
    }

    public void OnEnableCounter() {
        OnCheckChestCount();

        if (PlayerProgress.Instance.Player.ChestSet > 0)
        {
            holder.enabled = true;
            gameObject.SetActive(true);
        }
        else {
            OnDisableCounter();
        }
    }

}
