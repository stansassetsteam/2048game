﻿using UnityEngine;
using System.Collections;

public enum WS_RequestStatus  {
	Completed,
	Failed,
	Timeout
}
