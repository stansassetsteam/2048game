﻿using UnityEngine;
using System.Collections;

public class WS_Error  {

	private string _Describtion;

	public WS_Error(string describtion) {
		_Describtion = describtion;
	}

	public string Describtion {
		get {
			return _Describtion;
		}
	}
}
