using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class ST_WebServerManager : NonMonoSingleton<ST_WebServerManager> {



	private static bool _IsInitialized = false;

	public static event Action<string> PackageReceived = delegate {};

	public static event Action<Texture2D> CampaignImageLoaded = delegate {};
	public static event Action<string,string,string> CampaignTextLoaded = delegate {};

	public void Init() {
		if(_IsInitialized ) {return;}

		_IsInitialized = true;
	}


	
	//--------------------------------------
	// Get / Set
	//--------------------------------------

	public static bool IsInitialized {
		get {
			return _IsInitialized;
		}
	}


	//--------------------------------------
	// Public Methods
	//--------------------------------------

	//--------------------------------------
	// Responce Handler
	//--------------------------------------



	public static void HandlePackageError(WS_RequestResult RequestResult) { 
		Debug.Log ("HandlePackageError");

		PackageReceived(RequestResult.Package.Id);
	}
		
	public static void HandleResponce(WS_BaseServerPackage pack) {
		switch (pack.Id) {
		case WS_CompainRequest.PackId:
			
		//	int id = pack.GetDataField<int> ("id");
		//	int type = pack.GetDataField<int> ("type");
			string short_text = pack.GetDataField<string> ("short_text");
			string full_text = pack.GetDataField<string> ("full_text");
			string src = pack.GetDataField<string> ("src");
			string image = pack.GetDataField<string> ("image");

			CampaignTextLoaded (full_text, short_text, src);

			 SA.Common.Models.WWWTextureLoader imageLoader = SA.Common.Models.WWWTextureLoader.Create ();

			imageLoader.OnLoad += obj => CampaignImageLoaded (obj);

			imageLoader.LoadTexture (image);
		
			break;
		}

		PackageReceived(pack.Id);
	}


	
	//--------------------------------------
	// Hadnlers
	//--------------------------------------




}
