﻿using UnityEngine;


//using GAMiniJSON;
using BTS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;

public class ST_WebServer : Singletone<ST_WebServer> {

	public const string WEBSERVER_LOG_HEADER = "ST_WebServer Log: ";
	public const string SECRET = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDDzooiCyBcgbuNFeVCCF0/5dma";

	private const string WEBSERVER_VERSION = "1.0";

	private const string SERVER_URL = "game.beetheswarm.com/init";

	private const string WEBSERVER_ERROR_HEADER = "ST_WebServer Error: ";
	private const long  WEBSERVER_ALLOWED_REQUEST_TIME_RANGE = 900000;


	private static List<WS_BasePackage> DelayedPackages =  new List<WS_BasePackage>();

	private static string _RequestUrl = string.Empty;


	void Awake() {

		DontDestroyOnLoad(gameObject);

		_RequestUrl = "https://" +  SERVER_URL;

	

		#if UNITY_ANDROID && !UNITY_EDITOR


		if(AndroidNativeUtility.SDKLevel >= 23) {
			_RequestUrl = "http://" +  SERVER_URL;
		}

		#endif

	}

	//--------------------------------------
	// Public Methods
	//--------------------------------------



	public void Send(WS_BasePackage package) {
		if(package.AuthenticationRequired) {
			DelayedPackages.Add(package);
			Debug.LogWarning(package.Id +  " Caсhed");
			return;
		}

		SendRequest(package);
	}

	public void SendDelayedPackages() {
		Debug.Log("SendDelayedPackages" + DelayedPackages.Count);
		foreach(WS_BasePackage p in DelayedPackages) {
			SendRequest(p);
		}

		//DelayedPackages.Clear();
	}


	//--------------------------------------
	// Get / Set
	//--------------------------------------

	public static Int32 CurrentTimeStamp {
		get {
			return (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
		}
	}


	//--------------------------------------
	// Private Methods
	//--------------------------------------

	private void SendRequest(WS_BasePackage package) {

		WS_ServerRequest request =  WS_ServerRequest.Create();
		request.RequestCompleted += HandleRequestCompleted;
		request.Send(package, _RequestUrl);
		
	}


	void HandleRequestCompleted (WS_RequestResult result) {

		if(result.Status == WS_RequestStatus.Completed) {

			Debug.Log(WEBSERVER_LOG_HEADER + result.www.text);
			
			try {

				string serverVersion = result.www.responseHeaders["VERSION"];

				if(!serverVersion.Equals(WEBSERVER_VERSION)) {
										
					Debug.Log(WEBSERVER_ERROR_HEADER + "Version Validation FAILED.");
					ValidationFailed(result);
					return;
				} 


				//Dictionary<string, object> resp = GAMiniJSON.Json.Deserialize(result.www.text) as Dictionary<string, object>;
				long ServerTime = System.Convert.ToInt64(result.www.responseHeaders["TIME"]);
				
				
				long dif = CurrentTimeStamp - ServerTime;
				if(Math.Abs((int) dif)  > WEBSERVER_ALLOWED_REQUEST_TIME_RANGE ) {
					
					Debug.Log(WEBSERVER_ERROR_HEADER + "Time Validation FAILED.");
					ValidationFailed(result);
					return;
				} else {
					//Debug.Log("Time validation ok");
				}
				
				
				string ResponceHash = result.www.responseHeaders["SIQ"]; 
				string ClientHash = HMAC(SECRET, result.www.text); 
				
				
				
				if(!ClientHash.Equals(ResponceHash)) {
					Debug.Log(WEBSERVER_ERROR_HEADER + "Hash Validation FAILED.");
					ValidationFailed(result);
					return;
				} else {
					//Debug.Log("Hash validation ok");
				}
				


			
				WS_BaseServerPackage pack =  new WS_BaseServerPackage(result.www.text);
				if(pack.Error ==  null) {
					ST_WebServerManager.HandleResponce(pack);
				} else {
					ST_WebServerManager.HandlePackageError(result);
				}


				
				
			} catch(Exception ex) {
				Debug.LogError("Server response parsing failed");
				Debug.Log(ex.Message);
				ST_WebServerManager.HandlePackageError(result);
			}
			
			
			
		} else {
			if(result.Status == WS_RequestStatus.Timeout) {
				Debug.Log(WEBSERVER_ERROR_HEADER + "Request Failed by Timeout ");
			} else {
				Debug.Log(WEBSERVER_ERROR_HEADER + "Request Failed with error: " + result.www.error);
			}

			ST_WebServerManager.HandlePackageError(result);
		}
	}

	void ValidationFailed(WS_RequestResult result) {
		ST_WebServerManager.HandlePackageError(result);
	}


	public static string  HMAC(string key, string data) {
		var keyByte = ASCIIEncoding.UTF8.GetBytes(key);
		using (var hmacsha256 = new HMACSHA256(keyByte)) {
			hmacsha256.ComputeHash(ASCIIEncoding.UTF8.GetBytes(data));
			return ByteToString(hmacsha256.Hash);
		}
	}
	
	public static string ByteToString(byte[] buff) {
		string sbinary = "";
		for (int i = 0; i < buff.Length; i++)
			sbinary += buff[i].ToString("X2"); /* hex format */
		return sbinary.ToLower();
	}   


}
