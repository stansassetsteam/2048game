﻿using UnityEngine;

//using GAMiniJSON;
using BTS;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;

public class WS_ServerRequest : MonoBehaviour {


	public event Action<WS_RequestResult> RequestCompleted =  delegate {};
	private bool RequestReceived = false;


	private string _URL;
	private WS_BasePackage _Package;

	public static WS_ServerRequest Create() {
		GameObject g = new GameObject("WS_ServerRequest");
		g.transform.parent = ST_WebServer.Instance.gameObject.transform;


		return g.AddComponent<WS_ServerRequest>();
	}


	public void Send(WS_BasePackage package, string url) {
		_URL = url;
		_Package = package;
		StartCoroutine(SendRequest(package));

		if(package.Timeout > 0) {
			Invoke("Timeout", package.Timeout);
		}
	}


	public WS_BasePackage Package {
		get  {
			return _Package;
		}
	}


	private void Timeout() {

		if(RequestReceived) {
			return;
		}

		WS_RequestResult result  = new WS_RequestResult();
		result.www = null;
		result.Status = WS_RequestStatus.Timeout;
		result.Package = Package;
		
		RequestCompleted(result);
		CleanUp();
	}


	private IEnumerator SendRequest(WS_BasePackage package) {
	
		#if UNITY_3_5 || UNITY_4_0 || UNITY_4_0_1 || UNITY_4_1 || UNITY_4_2 || UNITY_4_3 
		Hashtable Headers = new Hashtable();
		#else
		Dictionary<string, string> Headers = new Dictionary<string, string>();
		#endif
		
		
		string RequestUrl = _URL;
		
		Dictionary<string, object> OriginalJSON =  new Dictionary<string, object>();
		OriginalJSON.Add("method", package.Id);
		OriginalJSON.Add("fields", package.GenerateData());
		
		
		
		
		string data = Json.Serialize(OriginalJSON);


		string hash = ST_WebServer.HMAC(ST_WebServer.SECRET, data); 

		Debug.Log(hash);
		
		byte[] binaryData = ASCIIEncoding.UTF8.GetBytes(data);

		Headers.Add("Content-Length", binaryData.Length.ToString());
		Headers.Add("Content-Type", "application/json");
		Headers.Add("siq", hash);

		switch(Application.platform) {
		case RuntimePlatform.IPhonePlayer:
			Headers.Add("platform", "1");
			break;
		default:
			Headers.Add("platform", "2");
			break;
		}
		
		
		Headers.Add("client_time", ST_WebServer.CurrentTimeStamp.ToString());
		Headers.Add("action_time", package.TimeStamp.ToString());
		
		
		
		Debug.Log("Sending: " + data);
		
		
		WWW www = new WWW(RequestUrl, binaryData, Headers);
		
		// Wait for download to complete
		yield return www;

		RequestReceived = true;

		WS_RequestResult result  = new WS_RequestResult();
		result.www = www;
		result.Package = Package;

		if(www.error == null) { 
			result.Status = WS_RequestStatus.Completed;
		} else {
			result.Status = WS_RequestStatus.Failed;
		}

		RequestCompleted(result);

		CleanUp();
	}


	private void CleanUp() {
		Destroy(gameObject);
	}

}
