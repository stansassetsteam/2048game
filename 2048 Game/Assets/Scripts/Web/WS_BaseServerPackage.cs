﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using BTS;

public class WS_BaseServerPackage  {

	private string _Id;
	private WS_Error _Error = null;
	private Dictionary<string, object> _Data = null;

	private string _RawResponce;
	
	//--------------------------------------
	//  Initialization
	//--------------------------------------
	
	
	public WS_BaseServerPackage(string rawResponce) {
		_RawResponce = rawResponce;
		Dictionary<string, object>  dict = Json.Deserialize(rawResponce) as Dictionary<string, object>;
		_Id = (string) dict["method"];

		Dictionary<string, object> errorData = (Dictionary<string, object>) dict["error"];
		bool HasError = (bool) errorData["status"];
		if(HasError) {
			_Error =  new WS_Error((string) errorData["message"]);
			Debug.Log(ST_WebServer.WEBSERVER_LOG_HEADER + "Package " + _Id + " Failed: " + _Error.Describtion);
		} else  {
			if( dict["data"] is Dictionary<string, object>) {
				_Data = (Dictionary<string, object>) dict["data"];

			}
			Debug.Log(ST_WebServer.WEBSERVER_LOG_HEADER + "Package " + _Id + " Received");
		}


	}



	//--------------------------------------
	//  Public Methods
	//--------------------------------------

	public T GetDataField<T>(string key) {



		T value = default(T);

		if(_Data.ContainsKey(key)) {
			object data = _Data[key];
			value = (T)System.Convert.ChangeType (data, typeof(T));
		}


		return value;
	}

	//--------------------------------------
	//  Get / Set
	//--------------------------------------


	public bool IsFailed {
		get {
			return Error != null;
		}
	}

	public string RawResponce {
		get {
			return _RawResponce;
		}
	}

	public WS_Error Error {
		get {
			return _Error;
		}
	}

	public string Id {
		get {
			return _Id;
		}
	}
}
