﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FPSCounter : Singletone<FPSCounter> {

	public Text FpsTimer;

	private int allFps = 0;
	private float afps = 0.0f;
	private float   totalFps = 0f;
	private float accum   = 0f; // FPS accumulated over the interval
	private int   frames  = 0; // Frames drawn over the interval
	private Color color = Color.white; // The color of the GUI, depending of the FPS ( R < 10, Y < 30, G >= 30 )
	private string sFPS = ""; // The fps formatted into a string.
	public  float frequency = 0.5F; // The update frequency of the fps
	public int nbDecimal = 1; // How many decimal do you want to display

	void Awake() {
		DontDestroyOnLoad (gameObject);
		Init ();
	}

	// Use this for initialization
	void Start () {
		StartCoroutine( FPS() );
	}
	
	// Update is called once per frame
	void Update () {
		accum += Time.timeScale/ Time.deltaTime;
		++frames;
	}

	public void Init() {
		if (FpsTimer == null) {
			GameObject prefab = GameObject.Instantiate(Resources.Load ("Prefabs/FPSCounterUI")) as GameObject;
			prefab.transform.SetParent(gameObject.transform);
			FpsTimer = prefab.GetComponentInChildren<Text> ();
		}
	}

	private IEnumerator FPS()
	{
		// Infinite loop executed every "frenquency" secondes.
		while( true )
		{
			// Update the FPS
			float fps = accum/frames;
			if (!System.Single.IsNaN(fps)) {
				++allFps;
				totalFps = fps + totalFps;
				afps = totalFps / allFps;
			}
			
			//Update the color
			color = (fps >= 30) ? Color.green : ((fps > 10) ? Color.red : Color.yellow);
			
			accum = 0.0F;
			frames = 0;
			
			sFPS = fps.ToString( "f" + Mathf.Clamp( nbDecimal, 0, 10 ) ) + "/" + afps.ToString( "f" + Mathf.Clamp( nbDecimal, 0, 10 ) );
			FpsTimer.color = color;
			FpsTimer.text = sFPS;
			
			yield return new WaitForSeconds( frequency );
		}
	}
}
