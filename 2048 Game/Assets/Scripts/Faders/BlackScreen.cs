﻿using UnityEngine;
using System.Collections;
using System;

public class BlackScreen : Singletone<BlackScreen> {

	public UnityEngine.UI.Image img;

	public Action WhenScreenIsBlack = delegate {};

	private Color black;

	void Awake() {
		black.a = 0f;
		black.r = 0f;
		black.g = 0f;
		black.b = 0f;

		img.gameObject.SetActive (false);
	}

	public void FadeInAndOut(Action ActionToDo) {
		img.gameObject.SetActive (true);
		WhenScreenIsBlack = ActionToDo;
		StartCoroutine (ToBlackFull());
	}

	IEnumerator ToBlackFull() {
		while (black.a < 1f) {
			black.a += 0.2f;
			img.color = black;
			
			yield return null;
		}
		black.a = 1f;
		img.color = black;

		WhenScreenIsBlack ();

		yield return StartCoroutine (ToTransparentFull());
	}
	
	IEnumerator ToTransparentFull() {
		while (black.a > 0f) {
			black.a -= 0.2f;
			img.color = black;
			
			yield return null;
		}

		black.a = 0f;
		img.color = black;

		img.gameObject.SetActive (false);
	}
}
