﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UIPanelController : MonoBehaviour {

    public static event Action CheckChestCount = delegate { };

   [SerializeField] private Animator m_animator;
    

    void Start()
    {
       // m_gameManager.OnRewardScreenShowed += ShowRewardBeesScreen;
      //  m_gameManager.OnRewardScreenClosed += HideRewardBeesScreen;
        m_animator = GetComponent<Animator>();
    }

    public void ShowRewardBeesScreen() {

         m_animator.SetTrigger("OpenRewardBeesScreen");
    }

    public void HideRewardBeesScreen() {
        CheckChestCount();
        m_animator.SetTrigger("HideRewardBeesScreen");    
    }

}
