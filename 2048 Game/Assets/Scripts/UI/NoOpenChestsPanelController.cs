﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoOpenChestsPanelController : MonoBehaviour {

    [SerializeField] private Animator m_animator;

    void Start()
    {
        m_animator = GetComponent<Animator>();   
    }

    public void ShowNoOpenChestsScreen() {
        m_animator.SetTrigger("FadeIn");
    }

    public void HideNoOpenChestsScreen() {
        m_animator.SetTrigger("FadeOut");
    }
}
