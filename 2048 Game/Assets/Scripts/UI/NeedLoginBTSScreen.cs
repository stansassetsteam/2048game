﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeedLoginBTSScreen : SingletonPrefab<NeedLoginBTSScreen> {
       

    Animator m_btsLoginForRewardsAnimator;

    void Awake()
    {
        m_btsLoginForRewardsAnimator = GetComponent<Animator>();   
    }

    public void Show() {
        m_btsLoginForRewardsAnimator.SetTrigger("FadeIn");
    }

    public void Hide() {
        m_btsLoginForRewardsAnimator.SetTrigger("FadeOut");
    }

    public void OnLoginBtsButtonClick()
    {
        m_btsLoginForRewardsAnimator.SetTrigger("FadeOut");
        BTSPanelController.Instance.Show();
       
    }
}
