﻿using BTS;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BTSPanelController : SingletonPrefab<BTSPanelController> {


    Animator m_btsPanelControllerAnimator;

    void Awake()
    {
        m_btsPanelControllerAnimator = GetComponent<Animator>();    
    }

    public void Show() {
        m_btsPanelControllerAnimator.SetTrigger("FadeIn");
    }

    public void Hide() {
        m_btsPanelControllerAnimator.SetTrigger("FadeOut");
    }

    public void OnBTSButtonClickFromMenu()
    {
            
            BTSPlugin.Show();
            m_btsPanelControllerAnimator.SetTrigger("FadeOut");
    }


    public void GoToBtsApp()
    {
#if UNITY_ANDROID
        Debug.Log(AndroidNativeUtility.Instance.ToString());
        AndroidNativeUtility.OnPackageCheckResult += OnPackageCheckResultCallback;
        AndroidNative.isPackageInstalled("com.beetheswarm.app");
#elif UNITY_IOS
		if (SA.IOSNative.System.SharedApplication.CheckUrl ("beetheswarm://")) {
			SA.IOSNative.System.SharedApplication.OpenUrl ("beetheswarm://");
		} else {
			SA.IOSNative.System.SharedApplication.OpenUrl ("itms-apps://itunes.apple.com/us/app/bee-the-swarm/id1019379941?mt=8");
		}
#endif
    }


    private void OnPackageCheckResultCallback(AN_PackageCheckResult result)
    {
        if (result.IsSucceeded)
        {
            AndroidNative.LaunchApplication("com.beetheswarm.app", string.Empty);
        }
        else
        {
            Application.OpenURL("https://play.google.com/store/apps/details?id=com.beetheswarm.app");
        }
    }
}
