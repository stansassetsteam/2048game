﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdsController : SA.Common.Pattern.Singleton<AdsController> {

    private const string NOADS_ID = "remove_ads";
    private const int _videoPercent = 30;

    private bool _isShowAds = true;

    public void Init() {

        CheckAds();

        SA.UltimateAds.Video.Init();
        SA.UltimateAds.Interstitial.Init();
        SA.UltimateAds.Banners.Init();

        UM_InAppPurchaseManager.instance.OnPurchaseFinished += OnPurchaseFinishedHandler;

    }

     void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void Disable()
    {

        Debug.Log("ADS DISABLED");
        _isShowAds = false;
        PlayerPrefs.SetInt("AdsDisabled", 1);

    }

    private bool CheckAds()
    {
        if (PlayerPrefs.HasKey("AdsDisabled"))
        {
            return (_isShowAds = false);
        }
        return true;
    }

    public void ShowBanner()
    {
#if UNITY_ANDROID || UNITY_IOS || UNITY_EDITOR

        if (!_isShowAds)
            return;

        Debug.Log("Show banner called");
        SA.UltimateAds.Banners.Show();
#endif
    }


    public void HideBanner()
    {


        if (!_isShowAds)
            return;

        Debug.Log("Hide banner called");
        SA.UltimateAds.Banners.Destroy();

    }
    public void VideoInterstitialShow()
    {


        if (!_isShowAds)
            return;

        int randomValue = UnityEngine.Random.Range(1, 101);
        Debug.Log("randomvalue:" + randomValue);
        int randomShowAds = UnityEngine.Random.Range(1, 101);
        Debug.Log("randomShowAds:" + randomShowAds);


        if (randomValue <= _videoPercent && SA.UltimateAds.Video.IsVideoReady())
        {
            SA.UltimateAds.Video.Show();
        }
        else
        {
            if (SA.UltimateAds.Interstitial.IsReady())
            {
                SA.UltimateAds.Interstitial.Show();
            }
        }

    }

    void OnPurchaseFinishedHandler(UM_PurchaseResult result)
    {
        if (result.isSuccess && result.product.id == NOADS_ID)
        {
            UM_InAppPurchaseManager.Instance.OnPurchaseFinished -= OnPurchaseFinishedHandler;

            Disable();
        }
    }
}

