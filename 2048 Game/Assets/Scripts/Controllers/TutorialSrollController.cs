﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TutorialSrollController : MonoBehaviour {

	public float MoveSpeed = 10f;
	public RectTransform CenterRect;
	public TutorialDotController DotsController;

	public ScrollRect Scroll;

	public List<GameObject> TutorialPanels = new List<GameObject> ();

	private Vector3 TouchPoint = new Vector3(0f,0f,0f);
	private RectTransform Current;

	private int CurrentElementIndex;

	void Start() {
		InputKeyboardController.MenuButtonRight += delegate() {
			if (CurrentElementIndex > 0) {
				CurrentElementIndex -= 1;

				MoveTo (TutorialPanels [CurrentElementIndex]);
			} else {
				MoveTo (TutorialPanels [0]);
			}
		};
			
		InputKeyboardController.MenuButtonLeft += delegate() {
			if (CurrentElementIndex < 2) {
				CurrentElementIndex += 1;

				MoveTo (TutorialPanels [CurrentElementIndex]);
			} else {
				MoveTo (TutorialPanels [2]);
			}
		};
	}

	void OnEnable() {
		Scroll.content.transform.position = CenterRect.position;
		DotsController.SetActiveDot (0);

		CurrentElementIndex = 0;
	}

	public void PointDown() {
		TouchPoint = Input.mousePosition;
	}

	public void PointUp() {
		Vector3 PointUp = Input.mousePosition;

		float distanceX = TouchPoint.x - PointUp.x;

		if (Mathf.Abs (distanceX) < GameConfig.AllowableClickError ){
			return;
		}

		if (distanceX > 0) {
			if (CurrentElementIndex < 2) {
				CurrentElementIndex += 1;

				MoveTo (TutorialPanels [CurrentElementIndex]);
			} else {
				MoveTo (TutorialPanels [2]);
			}

		} else {
			if (CurrentElementIndex > 0) {
				CurrentElementIndex -= 1;

				MoveTo (TutorialPanels [CurrentElementIndex]);
			} else {
				MoveTo (TutorialPanels [0]);
			}
		}
	}

	private void MoveTo(GameObject target) {
		DotsController.SetActiveDot (TutorialPanels.IndexOf(target));
		float currentDiff = CenterRect.position.x - target.transform.position.x;

		Vector3 targetPoint = Vector3.zero;

		targetPoint = new Vector3(Scroll.content.transform.position.x+currentDiff, Scroll.content.transform.position.y, Scroll.content.transform.position.z);

		if (gameObject.activeInHierarchy) {
			StartCoroutine(MovePart(targetPoint));
		}
	}

	private IEnumerator MovePart(Vector3 targetPos) {
		float time = 0;

		while (time < 1) {
			Scroll.content.transform.position = Vector3.Lerp(Scroll.content.transform.position, targetPos, time);

			time += 0.1f;

			yield return null;
		}
	}

}
