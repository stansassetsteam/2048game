﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class RowData {
	public GameObject[] Row;
}

public class ButtonIndexer {
	public int Row;
	public int Col;

	public ButtonIndexer() {
		Row = 0;
		Col = 0;
	}
}

public class KeyboardMenuController : MonoBehaviour {
	
	public RowData[] Column;

	private int RowMaxIndex = 0;
	private int ColumnMaxIndex = 0;

	private ButtonIndexer Indexer = new ButtonIndexer();
	private GameObject ActiveButton = null;

	void Awake() {
		Init ();
		ConnectEvents ();
	}

	void OnEnable() {
		ActivateButton (Indexer);
	}

	private void Init() {
		RowMaxIndex = Column [0].Row.Length - 1;
		ColumnMaxIndex = Column.Length - 1;
	}

	private void ConnectEvents() {
		InputKeyboardController.ConfirmButton += delegate() {
			Confirm();
		};

		InputKeyboardController.MenuButtonUp += delegate() {
			MoveUp();
		};

		InputKeyboardController.MenuButtonDown += delegate() {
			MoveDown();
		};

		InputKeyboardController.MenuButtonLeft += delegate() {
			MoveLeft();
		};

		InputKeyboardController.MenuButtonRight += delegate() {
			MoveRight();
		};
	}

	private void MoveLeft() {
		if (Indexer.Col > 0) {
			Indexer.Col--;

			ActivateButton (Indexer);
		}
	}

	private void MoveRight() {
		if (Indexer.Col < ColumnMaxIndex) {
			Indexer.Col++;

			ActivateButton (Indexer);
		}
	}

	private void MoveUp() {
		if (Indexer.Row > 0) {
			Indexer.Row--;

			ActivateButton (Indexer);
		}
	}

	private void MoveDown() {
		if (Indexer.Row < RowMaxIndex) {
			Indexer.Row++;

			ActivateButton (Indexer);
		}
	}

	private void Confirm() {

		if(GameMenuController.Instance.GameMenuPanel.activeInHierarchy || GameMenuController.Instance.GameOverPanel.activeInHierarchy) {
			if (ActiveButton != null) {
				Button btn = ActiveButton.GetComponent<Button> ();
				btn.onClick.Invoke ();
			}
		}


	}

	private void ActivateButton(ButtonIndexer indexer) {
		ActiveButton = Column [indexer.Col].Row [indexer.Row];

		for (int i = 0; i <  Column.Length; i++) {
			for (int j = 0; j < Column [i].Row.Length; j++) {
				Column [i].Row [j].GetComponent<Animator> ().SetTrigger ("Normal");
			}
		}

		ActiveButton.GetComponent<Animator> ().SetTrigger ("Highlighted");
	}
}
