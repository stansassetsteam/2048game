﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class ScoreController {
	private const string BEST_POINTS = "BEST_POINTS";//1
	private const string LAST_POINTS = "LAST_POINTS";//1
	private const string GOAL_POWER = "GOAL_POWER";//1

	private static int CurrentPoints = 0;

	public static event System.Action<int> CurrentPointsChangedAction = delegate {};
	public static event System.Action<int> BestPointsChangedAction = delegate {};

	public static bool ThisGameWasNewBestScore = false;

	private static Dictionary<int, int> _beesScoreDictionary;

	static ScoreController() {
		CurrentPoints = GetLastPoints ();

		_beesScoreDictionary = new Dictionary<int, int> ();
		_beesScoreDictionary.Add (500, 		1);
		_beesScoreDictionary.Add (1000, 	2);
		_beesScoreDictionary.Add (3000, 	3);
		_beesScoreDictionary.Add (5000, 	4);
		_beesScoreDictionary.Add (10000, 	5);
		_beesScoreDictionary.Add (20000, 	6);
		_beesScoreDictionary.Add (30000, 	7);
		_beesScoreDictionary.Add (40000, 	8);
		_beesScoreDictionary.Add (60000, 	9);
		_beesScoreDictionary.Add (100000, 	10);
	}

	public static void RewardBees () {
		Debug.Log ("GAME REWARD BY POINTS: " + CurrentPoints);

		int bees = 0;
		int points = 0;
		int rewardedScore = 0;

		foreach (KeyValuePair<int, int> pair in _beesScoreDictionary) {
			if (CurrentPoints >= pair.Key) {
				rewardedScore = pair.Key;
				bees = pair.Value;
				points = bees;
			} else {
				break;
			}
		}
		
	}

	public static int GetCurrentPoints() {
		return CurrentPoints;
	}

	public static void IncreaseGoalCount() {
		CurrentGoalPower += 1;
	}

	public static int GetCurrentGoalValue() {
		return 1 << CurrentGoalPower;
	}

	public static void AddToCurrentPoints(int addToPoints) {
		CurrentPoints += addToPoints;

		if (CurrentPoints > GetBestPoints()) {
			SetBestPoints (CurrentPoints);
		}

		CurrentPointsChangedAction (CurrentPoints);
	}

	public static void ResetCurrentPoints() {
		ThisGameWasNewBestScore = false;
		CurrentPoints = 0;

		CurrentPointsChangedAction (CurrentPoints);
	}

	public static int GetLastPoints() {
		if (PlayerPrefs.HasKey(LAST_POINTS)) {
			return PlayerPrefs.GetInt(LAST_POINTS);
		}

		return 0;
	}

	public static void SaveCurrentPoints() {
		PlayerPrefs.SetInt (LAST_POINTS, CurrentPoints);
	}

	public static void ClearCurrentPoints() {
		PlayerPrefs.DeleteKey (LAST_POINTS);
	}

	public static int GetBestPoints() {
		if (PlayerPrefs.HasKey(BEST_POINTS)) {
			return PlayerPrefs.GetInt(BEST_POINTS);
		}

		return 0;
	}

	public static void SetBestPoints(int points) {
		ThisGameWasNewBestScore = true;

		PlayerPrefs.SetInt (BEST_POINTS, points);
		BestPointsChangedAction (GetBestPoints ());
	}

	public static void ClearGoalPower() {
		PlayerPrefs.DeleteKey (GOAL_POWER);
	}

	private static int CurrentGoalPower {
		get{
			if (PlayerPrefs.HasKey(GOAL_POWER)) {
				return PlayerPrefs.GetInt (GOAL_POWER);
			}

			return GameConfig.FirstGoalPower;
		}

		set {
			PlayerPrefs.SetInt (GOAL_POWER, value);
		}
	}
}
