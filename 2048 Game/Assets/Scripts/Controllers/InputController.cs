﻿using UnityEngine;
using System.Collections;
using System;

public class InputController : MonoBehaviour {
	public static event Action<MoveDirections> MoveEvent = delegate {};

	private Vector3 TouchPoint = new Vector3(0f,0f,0f);


	void Awake() {
		ISN_GestureRecognizer.Instance.OnSwipe += delegate(ISN_SwipeDirection direction) {
            
			switch(direction) {
			case ISN_SwipeDirection.Down:
				MoveEvent (MoveDirections.Down);
				break;

			case ISN_SwipeDirection.Up:
				MoveEvent (MoveDirections.Up);
				break;

			case ISN_SwipeDirection.Left:
				MoveEvent (MoveDirections.Left);
				break;

			case ISN_SwipeDirection.Right:
				MoveEvent (MoveDirections.Right);
				break;
			}
		};
	}


	public void PointDown() {
		TouchPoint = Input.mousePosition;
	}

	public void PointUp() {
		Vector3 PointUp = Input.mousePosition;

		float distanceX = TouchPoint.x - PointUp.x;
		float distanceY = TouchPoint.y - PointUp.y;

		if (Mathf.Abs (distanceX) > Mathf.Abs (distanceY)) {
			if (Mathf.Abs (distanceX) < GameConfig.AllowableClickError ){
				return;
			}

			if (distanceX > 0) {
				MoveEvent (MoveDirections.Left);
			} else {
				MoveEvent (MoveDirections.Right);
			}
		} else {
			if (Mathf.Abs (distanceY) < GameConfig.AllowableClickError ){
				return;
			}

			if (distanceY > 0) {
				MoveEvent (MoveDirections.Down);
			} else {
				MoveEvent (MoveDirections.Up);
			}
		}
	}

	void Update() {
		if (Input.GetMouseButtonDown (0)) {
			PointDown ();
		}

		if (Input.GetMouseButtonUp (0)) {
			PointUp ();
		}
	}
}
