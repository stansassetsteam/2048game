﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CampaignOfADay : MonoBehaviour {

	public Text CompaignDayLongLabel;
	public Text CompaignDayShortLabel;
	public Image CompaignDayImage;

	private Texture2D LoadedImage = null;
	private string LoadedLongText = string.Empty;
	private string LoadedShortText = string.Empty;
	private string LoadedURL = string.Empty;

	private string CompaignDayURL = "http://www.beetheswarm.com/";

	void Awake() {
		ST_WebServerManager.CampaignImageLoaded += SetLoadedImage;
		ST_WebServerManager.CampaignTextLoaded += SetLoadedTexts;
	}

	void OnEnable() {
		UpdateContent ();
	}

	void OnDestroy() {
		ST_WebServerManager.CampaignImageLoaded -= SetLoadedImage;
		ST_WebServerManager.CampaignTextLoaded -= SetLoadedTexts;
	}

	public void GoToBeeTheSwarm() {
		Application.OpenURL(CompaignDayURL);
	}

	public void SetLoadedImage(Texture2D comImg) {
		LoadedImage = comImg;
	}

	public void SetLoadedTexts(string longText, string shortText, string comUrl) {
		LoadedLongText = longText;
		LoadedShortText = shortText;
		LoadedURL = comUrl;
	}

	public void UpdateText() {
		CompaignDayLongLabel.text = LoadedLongText;
		CompaignDayShortLabel.text = LoadedShortText;
		CompaignDayURL = LoadedURL;
	}

	public void UpdateContent() {
		if (LoadedImage != null) {
			CompaignDayImage.overrideSprite = Sprite.Create (LoadedImage, new Rect (0, 0, LoadedImage.width, LoadedImage.height), new Vector2 (0.5f, 0.5f));

			UpdateText ();
		}
	}
}
