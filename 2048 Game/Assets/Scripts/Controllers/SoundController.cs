﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum Sounds {
	none,
	merge,
	swipe,
	win2048,
	loose,
	button_click,
	intro_song,
	campaign_screen,
    shuffle_chests,
    all_chests_rewards,
    receive_chest
}

public class SoundController : Singletone<SoundController> {
	
	public List<AudioClip> SoundList = new List<AudioClip> ();
	private Dictionary<string, AudioSource> AudioSourceDictionary = new Dictionary<string, AudioSource> ();

	void Awake() {
		ParseSoundList ();
	}

	public void PlaySound(Sounds playSound) {
		AudioSourceDictionary [playSound.ToString ()].Play ();
	}

	public void StopSound(Sounds playSound) {
		AudioSourceDictionary [playSound.ToString ()].Stop ();
	}

	private void ParseSoundList() {
		foreach(var sound in SoundList) {
			AudioSource newSource = gameObject.AddComponent<AudioSource> ();
			newSource.clip = sound;

			AudioSourceDictionary.Add (sound.name, newSource);
		}
	}
}
