﻿using UnityEngine;
using System.Collections;


public class ServicesController : MonoBehaviour {

	private static bool ConnectionAttemptMade = false;

	void Start () {
		if(ConnectionAttemptMade) {
			return;
		}

		ConnectionAttemptMade = true;
		UM_GameServiceManager.Instance.Connect();
		UM_GameServiceManager.OnPlayerConnected += UM_GameServiceManager_OnPlayerConnected;
        
        //SA.Analytics.Unified.Manager.Instance.Init();
	}

	void OnDestroy() {
		UM_GameServiceManager.OnPlayerConnected -= UM_GameServiceManager_OnPlayerConnected;
		UM_GameServiceManager.ActionScoresListLoaded -= UM_GameServiceManager_ActionScoresListLoaded;
	}

	void UM_GameServiceManager_OnPlayerConnected () {
		UM_GameServiceManager.OnPlayerConnected -= UM_GameServiceManager_OnPlayerConnected;





		UM_GameServiceManager.ActionScoresListLoaded += UM_GameServiceManager_ActionScoresListLoaded;
		UM_GameServiceManager.Instance.LoadPlayerCenteredScores(GameConfig.SCORES_LEADERBOARD_ID, 2, UM_TimeSpan.ALL_TIME, UM_CollectionType.GLOBAL);
	}




	public static void SubmitScore(int score) {
		if(!UM_GameServiceManager.Instance.IsConnected) {
			return;
		}

		UM_GameServiceManager.Instance.SubmitScore(GameConfig.SCORES_LEADERBOARD_ID, score);
	}

	void UM_GameServiceManager_ActionScoresListLoaded (UM_LeaderboardResult res) {

		Debug.Log("ServicesController::UM_GameServiceManager_ActionScoresListLoaded: ");

		UM_GameServiceManager.ActionScoresListLoaded -= UM_GameServiceManager_ActionScoresListLoaded;

		UM_Leaderboard lb = UM_GameServiceManager.Instance.GetLeaderboard(GameConfig.SCORES_LEADERBOARD_ID);
		UM_Score bestScore = lb.GetCurrentPlayerScore(UM_TimeSpan.ALL_TIME, UM_CollectionType.GLOBAL);
		if(bestScore != null) {
			if(bestScore.LongScore >= ScoreController.GetBestPoints()) {
				ScoreController.SetBestPoints((int) bestScore.LongScore);
			} else {
				SubmitScore(ScoreController.GetBestPoints());
			}

			Debug.Log("GC best score: " + bestScore.LongScore);
		}
	}

}
