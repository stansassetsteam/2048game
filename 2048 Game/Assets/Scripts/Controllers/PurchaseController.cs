﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PurchaseController : MonoBehaviour {
	public GameObject ButtonRestore;

	public GameObject RemoveAdsButton;
	public Text RemoveAdsButtonText;

	void Start() {
		ST_BillingManager.SuccesPurchaseEvent += CheckAdBlockState;

		CheckAdBlockState ();
	}

	private void CheckAdBlockState() {
		if (PlayerData.Instance.AdsBLockedByPurchase) {
			RemoveAdsButton.SetActive (false);
			ButtonRestore.SetActive (false);
		} else {
			ButtonRestore.SetActive (true);
			RemoveAdsButton.SetActive (true);
			RemoveAdsButtonText.text = "REMOVE ADS ("+ST_BillingManager.Instance.GetProductPrice(PurchaseProducts.remove_ads)+")";
		}

		if (Application.platform == RuntimePlatform.Android) {
			ButtonRestore.SetActive (false);
		}
	}
}
