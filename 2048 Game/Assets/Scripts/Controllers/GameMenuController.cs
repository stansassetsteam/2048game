﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using BTS;

public class GameMenuController : SA_Singleton<GameMenuController> {
	public Text CurrentPoints;
	public Text BestPoints;
	public Text DescriptionText;

	public Text OverPanelScoreLabel;
	public Text OverPanelScoreValue;

	public Image HighScoreButtonImage;

	public GameObject GamePlayPanel;
	public GameObject GameMenuPanel;
	public GameObject GameOverPanel;
	public GameObject GameTutorPanel;
//	public GameObject CampaignOfADay;
	public Text SoundStateLabel;

	private int BannerId = 0;
	private const int _videoPercent = 30;

	private bool IsReturningFromVideAd = false;

	private Color32 GreyColor = new Color32(36, 36, 36, 255);
	private Color32 RedColor = new Color32(107, 17, 18, 255);

    private GameObject m_currentPanel;
    private GameObject m_noChestsPanel;
    private UIPanelController m_currentPanelController;
    private UIPanelController m_rewardChestsOpen;
    private NoOpenChestsPanelController m_currentNoChestsPanelController;

    [SerializeField] public GameObject m_chestsInBts;
    [SerializeField] public GameObject m_dropChanceInBts;

    void Awake() {
		//DeleteGamePlayerPrefs ();
		Init ();
		ConnectGameEvents ();
        		
        AdsController.Instance.Init();
		}

	void Start() {
        CheckConnectBTS();
        CurrentPoints.text = ScoreController.GetLastPoints ().ToString();
		BestPoints.text = ScoreController.GetBestPoints ().ToString();
		SetGoalText ();

//		ShowCampaighnOfADay ();
		CheckFirstStart ();
	}

	void OnApplicationFocus(bool focusStatus) {
		if (focusStatus) {
			if (IsReturningFromVideAd) {
				//ShowCampaighnOfADay ();
			} else {
				IsReturningFromVideAd = true;
			}
		}
	}

	private void DeleteGamePlayerPrefs () {
		PlayerPrefs.DeleteKey ("LAST_POINTS");
		PlayerPrefs.DeleteKey ("GOAL_POWER");

		for (int c = 0; c < 16; c++) {
			PlayerPrefs.DeleteKey ("Cell_" + c.ToString ());
		}
	}

	private void Init() {
		Application.targetFrameRate = 60;
		//AdMediation.Instance.Init ();
		//AdMediation.Instance.LoadVideo ();
//		GoogleMobileAd.Init ();
//		GoogleMobileAd.LoadInterstitialAd ();

		ST_WebServer.Instance.Send(new WS_CompainRequest());
//		UM_AdManager.Init(); 

		SA.UltimateAds.Interstitial.Init();
		SA.UltimateAds.Video.Init();
		ST_BillingManager.Instance.Init ();

//	#if UNITY_ANDROID
//    	JukkoSdk.Instance.Init ("3fd59b15-c20c-4e1d-9e0e-c1420ff97d34");
//    #elif UNITY_IOS
		//UM_AdManager.Init(); 
//		JukkoSdk.Instance.DebugMode = true;
//		JukkoSdk.Instance.Init ("1bff3764-e5e6-4209-aefe-26bc14b27f8a");
//	#endif


		CheckAndSetSound ();

		//OneSignal.Init("44116f34-3d2a-4d4a-8d0e-563828294f9d", "439232273822", HandleNotification);
		GoogleCloudMessageService.Instance.Init ();
	}

	private static void HandleNotification(string message, Dictionary<string, object> additionalData, bool isActive) {
	
	}

	private void ConnectGameEvents() {
		InputKeyboardController.EscapeButton += delegate() {
			if (!GameMenuPanel.activeInHierarchy) {
				ButtonMenuClickHandler();
			}
		};

		InputKeyboardController.ConfirmButton += delegate() {

			Debug.Log("ConfirmButton Pressed");

			if (GameTutorPanel.activeInHierarchy) {
				ButtonLetsPlayHandler();
				return;
			}

//			if (CampaignOfADay.activeInHierarchy) {
//				HideCampaighnOfADay();
//				return;
//			}


		};
		
		ScoreController.CurrentPointsChangedAction += delegate(int newPoints) {
			CurrentPoints.text = newPoints.ToString();
		};

		ScoreController.BestPointsChangedAction += delegate(int newPoints) {
			BestPoints.text = newPoints.ToString();
		};

		GameController.GameLosed += delegate {
			if (GameOverPanel.activeInHierarchy) {
				return;
			}

//			if (ScoreController.ThisGameWasNewBestScore) {
//				OverPanelScoreLabel.text = "NEW HIGH SCORE";
//				HighScoreButtonImage.color = RedColor;
//			} else {
//				OverPanelScoreLabel.text = "YOUR SCORE";
//				HighScoreButtonImage.color = GreyColor;
//			}
			//ScoreController.RewardBees ();

			OverPanelScoreValue.text = ScoreController.GetCurrentPoints ().ToString();

			ServicesController.SubmitScore(ScoreController.GetCurrentPoints());
			Show(GameOverPanel);

            ShowVideAd();
			try {
				Invoke("ShowRateUsOnOverScreen", 1f);
			} catch (Exception e) {}
		};

		FilledCell.GoalReached += delegate() {
			SetGoalText();
		};
	}

	/*private void ShowRateUsOnOverScreen() {
		if (PlayerData.Instance.IsRated) {
			return;
		}

		Debug.Log ("ShowRateUsOnOverScreen");
		MobileNativeDialog InviteDialog = new MobileNativeDialog("Bee The Swarm", "Do you like playing 2048?");
		InviteDialog.OnComplete += OnRateUsDialogClose;
	}*/

	private void OnRateUsDialogClose(MNDialogResult res) {
		Debug.Log ("ShowRateUs res "+res.ToString());
		switch (res) {
		case MNDialogResult.YES:
			PlayerData.Instance.IsRated = true;
			ButtonRateClickHandler ();

			break;
		case MNDialogResult.NO:
			UM_ShareUtility.SendMail ("I Have an Issue with 2048", "I have an issue with 2048 because ...", "developer@beetheswarm.com");

			break;
		}
	}

	public void ButtonReturnClickHandler() {
		BlackScreen.Instance.FadeInAndOut (delegate {
            CheckConnectBTS();
			Show(GamePlayPanel);
			//ShowCampaighnOfADay();
		});
	}

	public void ButtonNewGameClickHandler() {
		BlackScreen.Instance.FadeInAndOut (delegate {
			Show(GamePlayPanel);
            CheckConnectBTS();
			GameController.Instance.StartNewGame();

			SetGoalText();

			IsReturningFromVideAd = false;

			ShowVideAd();

			PlayerData.Instance.NewGameCounter += 1;

			/*if ((PlayerData.Instance.NewGameCounter % 10) == 0) {
				MobileNativeDialog InviteDialog = new MobileNativeDialog("Bee The Swarm", "Do you want to help us and invite your friends?");
				InviteDialog.OnComplete += OnInviteDialogClose;
			}*/
		});
	}

	private void OnInviteDialogClose(MNDialogResult res) {
		if (res == MNDialogResult.YES) {
			if(Application.platform == RuntimePlatform.Android) {
				GP_AppInviteBuilder builder =  new GP_AppInviteBuilder("Hey! I love playing this game 2048 by Bee The Swarm, it helps raise money for charities just by playing");
				builder.SetDeepLink(GameConfig.PLAY_STORE_URL);
				builder.SetMessage(GameConfig.INVITE_TEXT);
				GP_AppInvitesController.Instance.StartInvitationDialog(builder);
			}  else  {
				GK_FriendRequest r =  new GK_FriendRequest();
				r.Send();
			}
		}
	}

	public void ButtonHowPlayClickHandler() {
		BlackScreen.Instance.FadeInAndOut (delegate {
            Show(GameTutorPanel);
		});
	}

	public void ButtonRateClickHandler() {
		switch(Application.platform) {
		case RuntimePlatform.Android:
			AndroidNativeUtility.OpenAppRatingPage(GameConfig.PLAY_STORE_URL);
			break;
		case RuntimePlatform.IPhonePlayer:
			IOSNativeUtility.RedirectToAppStoreRatingPage();
			break;
		}

	}

	public void ButtonRemoveAdsClickHandler() {
		ST_BillingManager.Instance.Purchase (PurchaseProducts.remove_ads);

	}

	public void ButtonSoundOnOff() {
		PlayerData.Instance.IsSound = !PlayerData.Instance.IsSound;
		CheckAndSetSound ();
	}

	private void CheckAndSetSound() {
		if (PlayerData.Instance.IsSound) {
			SoundStateLabel.text = "SOUND ON";
			AudioListener.volume = 1;

			SoundController.Instance.PlaySound (Sounds.button_click);
		}  else {
			SoundStateLabel.text = "SOUND OFF";
			AudioListener.volume = 0;
		}
	}

	public void ButtonRestoreClickHandler() {
		ST_BillingManager.Instance.RestorePurchases ();
	}

	public void ButtonShareClickHandler() {
		/*string message = string.Format(GameConfig.SHARE_TEXT, ScoreController.GetCurrentPoints());

		UM_ShareUtility.ShareMedia(GameConfig.SHARE_TITLE, message);*/

	}

    public void OpenRewardBeesScreen()
    {
        if (!BTSPlugin.IsUserLoggedIn())
        {
            NeedLoginBTSScreen.Instance.Show();
        }
        else
        {
            if (PlayerProgress.Instance.Player.ChestSet > 0)
            {
                ShowPage("ChooseChest");
            }
            else
            {
                ShowNoChestsPage("NoOpenChests");
            }
        }
    }

    public void ShowPage(string key)
    {
        if (m_currentPanel != null)
        {
            if (m_currentPanel.name == key)
            {
                m_currentPanelController.ShowRewardBeesScreen();
                return;
            }
        }
        Destroy(m_currentPanel);
        Resources.UnloadUnusedAssets();
        m_currentPanel = Instantiate(Resources.Load("UI/" + key)) as GameObject;
        m_currentPanelController = m_currentPanel.GetComponent<UIPanelController>();
        m_currentPanelController.ShowRewardBeesScreen();

    }

    public void ShowNoChestsPage(string key)
    {
        if (m_noChestsPanel != null)
        {
            if (m_noChestsPanel.name == key)
            {
                m_currentNoChestsPanelController.ShowNoOpenChestsScreen();
                return;
            }
        }

        Destroy(m_noChestsPanel);
        Resources.UnloadUnusedAssets();
        m_noChestsPanel = Instantiate(Resources.Load("UI/" + key)) as GameObject;
        m_currentNoChestsPanelController = m_noChestsPanel.GetComponent<NoOpenChestsPanelController>();
        m_currentNoChestsPanelController.ShowNoOpenChestsScreen();
    }

    public void ButtonBTSClickHandler () {

        if (!BTSPlugin.IsUserLoggedIn())
        {
            BTSPanelController.Instance.Show();
        }
        else
        {
            BTSPlugin.Show();
        }
    }

	private void SetGoalText() {
		DescriptionText.text = string.Format( "Connect identical numbers and get to the {0} tile", ScoreController.GetCurrentGoalValue()); 
	}

	private IEnumerator PostScreenshot() {

		yield return new WaitForEndOfFrame();
		// Create a texture the size of the screen, RGB24 format
		int width = Screen.width;
		int height = Screen.height;
		Texture2D tex = new Texture2D( width, height, TextureFormat.RGB24, false );
		// Read screen contents into the texture
		tex.ReadPixels( new Rect(0, 0, width, height), 0, 0 );
		tex.Apply();

		string message = string.Format(GameConfig.SHARE_TEXT, ScoreController.GetCurrentPoints());

		UM_ShareUtility.ShareMedia(GameConfig.SHARE_TITLE, message, tex);

		Destroy(tex);

	}

	public void ButtonTryAgainClickHandler() {
		ButtonNewGameClickHandler ();
		StartCoroutine (PromoBTSCoroutine ());
	}

	public void ButtonInviteClickHandler() {
		if(Application.platform == RuntimePlatform.Android) {
			GP_AppInviteBuilder builder =  new GP_AppInviteBuilder("2048 - Bee The Swarm");
			builder.SetDeepLink(GameConfig.PLAY_STORE_URL);
			builder.SetMessage(GameConfig.INVITE_TEXT);
			GP_AppInvitesController.Instance.StartInvitationDialog(builder);
		} else  {
			GK_FriendRequest r =  new GK_FriendRequest()	;
			r.Send();
		}
	}

	public void ButtonLikeClickHandler() {
		Application.OpenURL(GameConfig.BTS_FB_PAGE);
	}

	public void ButtonMenuClickHandler() {
		BlackScreen.Instance.FadeInAndOut (delegate {
			Show(GameMenuPanel);
		});
	}

	public void ButtonLeaderboardClickHandler() {
		UM_GameServiceManager.Instance.ShowLeaderBoardUI(GameConfig.SCORES_LEADERBOARD_ID);
	}

	public void ButtonLetsPlayHandler() {
		BlackScreen.Instance.FadeInAndOut (delegate {
            CheckConnectBTS();
			Show(GamePlayPanel);
		});
	}

	private void CheckFirstStart() {
		if (PlayerData.Instance.IsFirstGameStart) {
			Show (GameTutorPanel);
		} else {
			if (!GameOverPanel.activeInHierarchy) {
				Show (GamePlayPanel);
			}

			//IsReturningFromVideAd = false;
			//ShowVideAd ();
		}
	}

//	public void ShowCampaighnOfADay() {
//		CampaignOfADay.SetActive (true);
//	}

//	public void HideCampaighnOfADay() {
//		CheckFirstStart ();
//		CampaignOfADay.SetActive (false);
//		SoundController.Instance.PlaySound (Sounds.button_click);
//	}

	private void Show(GameObject toShow) {
		GamePlayPanel.SetActive(false);
		GameMenuPanel.SetActive(false);
		GameOverPanel.SetActive(false);
		GameTutorPanel.SetActive(false);
//		CampaignOfADay.SetActive (false);
		toShow.SetActive (true);

		SoundController.Instance.PlaySound (Sounds.button_click);

		if (toShow == GameTutorPanel) {
			SoundController.Instance.PlaySound (Sounds.intro_song);
		} else {
			SoundController.Instance.StopSound (Sounds.intro_song);
		}

		if (toShow == GamePlayPanel) {
			ShowBannerAd ();
		} else {
			HideBannerAd ();
		}
	}

	private void ShowVideAd() {
		if (PlayerData.Instance.AdsBLockedByPurchase) 
			return;

		//AdMediation.Instance.ShowVideo();
		
//		JukkoSdk.Instance.ShowAd ((AdClosedEvent e) => {

        AdsController.Instance.VideoInterstitialShow();

//			handle AdClosedEvent here
//		});
//		
				
		
	}

	private void ShowBannerAd() {
        AdsController.Instance.ShowBanner();
		/*if (PlayerData.Instance.AdsBLockedByPurchase) {
			if (BannerId != 0) {
				UM_AdManager.DestroyBanner (BannerId);
				BannerId = 0;
			}
		} else {
			if (BannerId == 0) {
				BannerId = UM_AdManager.CreateAdBanner (TextAnchor.LowerCenter, GADBannerSize.SMART_BANNER);
			} else {
				if (UM_AdManager.IsBannerLoaded (BannerId) && !UM_AdManager.IsBannerOnScreen (BannerId)) {
					UM_AdManager.ShowBanner (BannerId);

				}
			}
		}*/
	}

    private void CheckConnectBTS()
    {
        if (BTSPlugin.IsUserLoggedIn())
        {
            m_chestsInBts.SetActive(true);
            m_dropChanceInBts.SetActive(true);
            BTSPlugin.GetChest(count => {
                PlayerProgress.Instance.Player.Chests = count;
            });
        }
        else
        {
            m_chestsInBts.SetActive(false);
            m_dropChanceInBts.SetActive(false);
        }
    }

    public void HideBannerAd() {
        AdsController.Instance.HideBanner();
		/*if (BannerId != 0) {
			if (UM_AdManager.IsBannerLoaded (BannerId) && UM_AdManager.IsBannerOnScreen (BannerId)) {
				UM_AdManager.HideBanner(BannerId);
			}
		}*/
	}

	IEnumerator	PromoBTSCoroutine(){
		yield return new WaitForSeconds (0.5f);
		PromoBTSController.Instance.ShowOurGamesPromo ();
	}
}
