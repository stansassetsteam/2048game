﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TutorialDotController : MonoBehaviour {

	public List <Image> ImageList = new List<Image> ();

	public Sprite DefaultSprite;
	public Sprite ActiveSprite;

	public void SetActiveDot(int number) {
		for (int i = 0; i < ImageList.Count; i++) {
			if (i == number) {
				ImageList [i].overrideSprite = ActiveSprite;
			} else {
				ImageList [i].overrideSprite = DefaultSprite;
			}
		}
	}
}
