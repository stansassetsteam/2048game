﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using BTS;
using UnityEngine.UI;

public enum MoveDirections {
	Up,
	Down,
	Left,
	Right
}

public class GameController : Singletone<GameController> {
	private int[][] GridArray = new int[4][]{
		new int[] {0,1,2,3},
		new int[] {4,5,6,7},
		new int[] {8,9,10,11},
		new int[] {12,13,14,15}
	};

    [SerializeField] Animator m_getAnimateChestAnimator;

	private bool GridArrayIsTransposed = false;

	[HideInInspector]
	public List<SingleCell> SingleCellList = new List<SingleCell>();

	public float MoveTime = 0.2f;

	public List<Color> ColorList = new List<Color>();
	public Dictionary<int,Color> ColorDictionary = new Dictionary<int,Color>();

	private bool GameInMove = false;
	private int PosibleMovesNumber = 0;

    public Text m_countDropChance;

    private float m_increaseChanceRate = 0.02f;
    private float m_startChanceRateChest = 1f;
    public float m_nextChanceRateChest;
    private float m_maxChanceRateChest = 50f;

    public static event Action GameLosed = delegate {};

	[HideInInspector]
	public bool WasAtLeastOneAction = false;

	void Awake() {
		InputController.MoveEvent += MoveExistingCells;
	}

	void Start() {

        if (BTSPlugin.IsUserLoggedIn())
        {
            BTSPlugin.GetChest(count => {
                PlayerProgress.Instance.Player.Chests = count;
            });
        }
            int num = 2;

        m_countDropChance.text = "" + m_startChanceRateChest;
        m_nextChanceRateChest = m_startChanceRateChest;

		foreach (Color color in ColorList) {
			ColorDictionary.Add (num, color);
			num *= 2;
		}

		SingleCellList.Sort ();

		if (HasSavedGame()) {
			RestoreLastGameProgress ();
		} else {
			StartNewGame ();
		}
	}

	void OnEnable() {
		GameInMove = false;
	}

	void OnDestroy() {
		InputController.MoveEvent -= MoveExistingCells;
	}
	///////////////
	/// Game Play
	///////////////

	private void UnlockGame() {
		GameInMove = false;
	}


	///////////////
	/// Movement and Coroutines
	///////////////

	private void MoveExistingCells(MoveDirections direction) {
		if (GameInMove || !GameMenuController.Instance.GamePlayPanel.activeInHierarchy) {
			return;
		}

		GameInMove = true;
		Invoke ("UnlockGame", GameConfig.MoveAnimationTime + 0.1f);

		switch (direction) {
		case MoveDirections.Up:
			TransposeMatrix();

			for (int row = 0; row < 4; row++) {
				for (int column = 0; column < 4; column++) {
					CheckingCellsForMove(row, column, direction);
				}
			}
			break;
		case MoveDirections.Down:
			TransposeMatrix();

			for (int row = 0; row < 4; row++) {
				for (int column = 3; column >= 0; column--) {
					CheckingCellsForMove(row, column, direction);
				}
			}
			break;
		case MoveDirections.Left:
			NormalMatrix ();

			for (int row = 0; row < 4; row++) {
				for (int column = 0; column < 4; column++) {
					CheckingCellsForMove(row, column, direction);
				}
			}
			break;
		case MoveDirections.Right:
			NormalMatrix ();

			for (int row = 0; row < 4; row++) {
				for (int column = 3; column >= 0; column--) {
					CheckingCellsForMove(row, column, direction);
				}
			}
			break;
		}

		bool WasAtLeastOneMergeAction = false;
		WasAtLeastOneAction = false;

		foreach (var cell in SingleCellList) {
			if (cell.CellNod != null) {
				if (cell.CellForMege != null) {
					MergeCellsAction (cell);

					WasAtLeastOneMergeAction = true;
				} else {
					MoveCellsAction (cell);
				}
			}
		}

		if (WasAtLeastOneAction) {
			Invoke ("TryToCreateNumber", GameConfig.MoveAnimationTime+0.1f);
		}

		if (WasAtLeastOneMergeAction) {
			WasAtLeastOneMergeAction = false;
		}

		Invoke ("SaveGameProgressToStorage",  GameConfig.MoveAnimationTime+0.2f);
		Invoke ("CheckAllCellsForAction",  GameConfig.MoveAnimationTime+0.3f);
	}

	private void MoveFilledCelTo(SingleCell from, SingleCell to) {
		to.CellNod = from.CellNod;
		to.CellNod.transform.SetParent (to.transform);

		from.CellNod = null;
	}

	private void MergeCells(SingleCell current, SingleCell merged) {
		merged.CellForMege = current.CellNod;
		merged.CellForMege.transform.SetParent (merged.transform);

		current.CellNod = null;
	}

	private void MoveCellsAction(SingleCell element) {
		element.transform.SetAsLastSibling ();
		element.CellNod.MoveToParrent ();
	}

	private void MergeCellsAction(SingleCell merged){
		merged.transform.SetAsLastSibling ();
		merged.CellNod.transform.SetAsLastSibling ();

				merged.CellNod.MoveToParrent ();

		merged.CellForMege.MoveAndMerge (merged);

        IncreaseChanceRateChest();
        m_countDropChance.text = "" + m_nextChanceRateChest;

        int spawnPercentage = UnityEngine.Random.Range(1, 101);
        Debug.Log("spawn percentage:" + spawnPercentage);
        if (spawnPercentage < m_nextChanceRateChest && BTSPlugin.IsUserLoggedIn()) {
            m_getAnimateChestAnimator.SetTrigger("ShowChest");
            BTSPlugin.AddChest(count => {
                PlayerProgress.Instance.Player.Chests = count;
            });
        }
        
    }

	private bool IsCellsCanBeMerged(SingleCell cel1, SingleCell cel2) {
		if (cel1.CellNod != null && cel2.CellNod != null) {
			
			if (cel1.CellForMege != null || cel2.CellForMege != null) {
				return false;
			}

			if (cel1.CellNod.NumberInside == cel2.CellNod.NumberInside) {
				return true;
			}
		}

		return false;
	}

	///////////////
	/// Opertions with cells
	///////////////

	private FilledCell CellIn(int row, int col) {
		return SingleCellList [GridArray [row] [col]].CellNod;
	}

	private int GetEmptyRandomCell() {
		List<int> EmptyCells = new List<int>();

		for (int i = 0; i < SingleCellList.Count; i++) { //creating array with empty cells
			if (SingleCellList[i].CellNod == null) {
				EmptyCells.Add(i);
			}
		}

		if (EmptyCells.Count == 0) {
			return -1;
		} else if (EmptyCells.Count == 1) {
			return EmptyCells [0];
		} else {
			for (int i = EmptyCells.Count - 1; i > 0; i--) { //randomize array with empty cells
				int r = UnityEngine.Random.Range (0, i);
				int tmp = EmptyCells [i];

				EmptyCells [i] = EmptyCells [r];
				EmptyCells [r] = tmp;
			}

			return EmptyCells [UnityEngine.Random.Range (0, EmptyCells.Count - 1)]; // return random empty cell
		}
	}

	private bool AllCellAreFull() {
		foreach (SingleCell cell in SingleCellList) {
			if (cell.CellNod == null) {
				return false;
			}
		}

		return true;
	}

	private void CreateNumberForNewGame() {
		int emptycell = GetEmptyRandomCell ();

		SingleCellList [emptycell].CreateNodIn ();
		SingleCellList [emptycell].CellNod.SetNumber (2);

		TryToCreateNumber ();

		Invoke ("SaveGameProgressToStorage",  GameConfig.MoveAnimationTime);
	}

	private void TryToCreateNumber() {
		int CellNumber = GetEmptyRandomCell ();

		if (CellNumber >= 0) {
			SingleCellList [CellNumber].CreateNodIn ();

			int[] arr = new int[5] {2,2,2,2,4};
			int rand = UnityEngine.Random.Range (0, 5);

			SingleCellList [CellNumber].CellNod.SetNumber (arr[rand]);
		}
	}

	#if UNITY_EDITOR
	void Update() {
		if (Input.GetKeyDown (KeyCode.W)) {
			MoveExistingCells (MoveDirections.Up);
		}

		if (Input.GetKeyDown (KeyCode.S)) {
			MoveExistingCells (MoveDirections.Down);
		}

		if (Input.GetKeyDown (KeyCode.A)) {
			MoveExistingCells (MoveDirections.Left);
		}

		if (Input.GetKeyDown (KeyCode.D)) {
			MoveExistingCells (MoveDirections.Right);
		}
	}
	#endif
	///////////////
	/// Init
	///////////////

	public void StartNewGame() {
		CleanGameProgress ();
		CleanGameProgressFromStorage ();
        m_nextChanceRateChest = 1f;
        m_countDropChance.text = "" + m_nextChanceRateChest;

        ScoreController.ResetCurrentPoints ();

		CreateNumberForNewGame ();
	}

	public void CleanGameProgress() {
		foreach (SingleCell cel in SingleCellList) {
			if (cel.CellNod != null) {
				
				DestroyImmediate (cel.CellNod.gameObject);
				cel.CellNod = null;
			}

			if (cel.CellForMege != null) {

				DestroyImmediate (cel.CellForMege.gameObject);
				cel.CellForMege = null;
			}
		}
	}

	public void RestoreLastGameProgress() {
		CleanGameProgress ();

		foreach (SingleCell cel in SingleCellList) {
			int cellNumber = PlayerPrefs.GetInt ("Cell_" + cel.CellIndex);

			if (cellNumber != 0) {
				SingleCellList [cel.CellIndex].CreateNodIn ();
				SingleCellList [cel.CellIndex].CellNod.SetNumber (cellNumber);
			}
		}
	}

	public void SaveGameProgressToStorage() {
		ScoreController.SaveCurrentPoints ();

		foreach (SingleCell cel in SingleCellList) {
			if (cel.CellNod == null) {
				PlayerPrefs.SetInt ("Cell_" + cel.CellIndex, 0);
			} else {
				PlayerPrefs.SetInt ("Cell_" + cel.CellIndex, cel.CellNod.NumberInside);
			}
		}
	}


	public void CleanGameProgressFromStorage() {
		ScoreController.ClearCurrentPoints();
		ScoreController.ClearGoalPower();

		foreach (SingleCell cel in SingleCellList) {
			PlayerPrefs.DeleteKey ("Cell_" + cel.CellIndex);
		}
	}

	public void AddCellTolist(SingleCell cell) {
		SingleCellList.Add (cell);
	}

	public bool HasSavedGame() {
		return PlayerPrefs.HasKey ("Cell_0");
	}

	public int GetMaxCellValue () {
		int maxValue = 0;
		foreach (SingleCell cell in SingleCellList) {
			if (cell.CellNod != null && cell.CellNod.NumberInside > maxValue) {
				maxValue = cell.CellNod.NumberInside;
			}
		}

		return maxValue;
	}

	private void CheckAllCellsForAction() {
		if (AllCellAreFull()) {
			PosibleMovesNumber = 0;

			for (int dir = 0; dir < 4; dir++) {
				if (dir < 3) {
					TransposeMatrix ();
				} else {
					NormalMatrix ();
				}

				for (int row = 0; row < 4; row++) {
					for (int column = 0; column < 4; column++) {
						CheckingCells (row, column, (MoveDirections)dir);
					}
				}
			}

			if (PosibleMovesNumber == 0) {
				//SA.Analytics.Unified.Manager.Instance.Init();
				CleanGameProgressFromStorage ();

				SoundController.Instance.PlaySound (Sounds.loose);

				GameLosed ();
			}
		}
	}

	private void NormalMatrix() {
		if (!GridArrayIsTransposed) {
			return;
		}

		GridArrayIsTransposed = false;

		int[][] localArr = new int[4][];

		for (int i = 0; i < 4; i++) {
			localArr[i] = new int[4];
		}

		for (int row = 0; row < 4; row++) {
			for (int column = 0; column < 4; column++) {
				localArr [column] [row] = GridArray [row] [column];
			}
		}

		GridArray = localArr;
	}

	private void TransposeMatrix() {
		if (GridArrayIsTransposed) {
			return;
		}

		GridArrayIsTransposed = true;

		int[][] localArr = new int[4][];

		for (int i = 0; i < 4; i++) {
			localArr[i] = new int[4];
		}

		for (int row = 0; row < 4; row++) {
			for (int column = 0; column < 4; column++) {
				localArr [row] [column] = GridArray [column] [row];
			}
		}

		GridArray = localArr;
	}

	private FilledCell GetElementAt(int row, int col ) {
		return CellIn (row, col);
	}

	private SingleCell GetCellAt(int row, int col) {
		return SingleCellList [GridArray [row] [col]];
	}

	private void CheckingCells(int line, int current, MoveDirections dir) {
		int mergeIndex = 0;
		bool IsMergedIndexExists = true;

		switch (dir) {
		case MoveDirections.Right:
		case MoveDirections.Down:
			mergeIndex = current + 1;
			break;
		case MoveDirections.Left:
		case MoveDirections.Up:
			mergeIndex = current - 1;
			break;
		}

		if (mergeIndex >= 4 || mergeIndex < 0) {
			IsMergedIndexExists = false;
		}

		if (GetElementAt (line, current) != null && IsMergedIndexExists) {
			SingleCell CurrentCell = GetCellAt (line, current);
			SingleCell NextCell = GetCellAt (line, mergeIndex);

			if (GetElementAt (line, mergeIndex) == null) {
				PosibleMovesNumber += 1;
				CheckingCellsForMove (line, mergeIndex, dir);
			}  else {
				if (IsCellsCanBeMerged(CurrentCell, NextCell)) {
					PosibleMovesNumber += 1;
					CheckingCellsForMove (line, mergeIndex, dir);
				}
			}
		}
	}

	private void CheckingCellsForMove(int line, int current, MoveDirections dir) {
		int mergeIndex = 0;
		bool IsMergedIndexExists = true;

		switch (dir) {
		case MoveDirections.Right:
		case MoveDirections.Down:
			mergeIndex = current + 1;
			break;
		case MoveDirections.Left:
		case MoveDirections.Up:
			mergeIndex = current - 1;
			break;
		}

		if (mergeIndex >= 4 || mergeIndex < 0) {
			IsMergedIndexExists = false;
		}

		if (GetElementAt (line, current) != null && IsMergedIndexExists) {
			SingleCell CurrentCell = GetCellAt (line, current);
			SingleCell NextCell = GetCellAt (line, mergeIndex);

			if (GetElementAt (line, mergeIndex) == null) {
				MoveFilledCelTo (CurrentCell, NextCell);
				CheckingCellsForMove (line, mergeIndex, dir);
			}  else {
				if (IsCellsCanBeMerged(CurrentCell, NextCell)) {
					MergeCells (CurrentCell, NextCell);
					CheckingCellsForMove (line, mergeIndex, dir);
				}
			}
		}
	}

    private void IncreaseChanceRateChest()
    {

        m_nextChanceRateChest = (float)System.Math.Round(m_nextChanceRateChest + m_increaseChanceRate, 2);

        if (m_nextChanceRateChest >= m_maxChanceRateChest)
        {
            m_nextChanceRateChest = m_maxChanceRateChest;
            Debug.Log("MAX CHANCE RATE");
        }
    }

    void OnApplicationQuit() {
		SaveGameProgressToStorage ();
	}
}