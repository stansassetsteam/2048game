﻿using UnityEngine;
using System.Collections;
using System;

public class InputKeyboardController : MonoBehaviour {
	public static event Action EscapeButton = delegate {};
	public static event Action ConfirmButton = delegate {};

	public static event Action MenuButtonUp = delegate {};
	public static event Action MenuButtonDown = delegate {};
	public static event Action MenuButtonLeft = delegate {};
	public static event Action MenuButtonRight = delegate {};



	void Start() {
		
		ISN_GestureRecognizer.Instance.OnSwipe += delegate(ISN_SwipeDirection direction) {
			switch(direction) {
			case ISN_SwipeDirection.Down:
				MenuButtonDown ();
				break;

			case ISN_SwipeDirection.Up:
				MenuButtonUp();
				break;

			case ISN_SwipeDirection.Left:
				MenuButtonLeft();
				break;

			case ISN_SwipeDirection.Right:
				MenuButtonRight();
				break;
			}
		};

	}
		

	void Update () {
		switch(Application.platform) {
		case RuntimePlatform.tvOS:
			ReadTvInput();
			#if UNITY_EDITOR_OSX
			UnityEngine.Apple.TV.Remote.allowExitToHome = GameMenuController.Instance.GameMenuPanel.activeInHierarchy;
			#endif

			break;
			default:
			ReadPCInput();
			break;
		}

	}

	private void ReadTvInput() {


		if(Input.GetKeyDown("joystick button 14")){
			ConfirmButton ();
		}

		if(Input.GetKeyDown("joystick button 15")){
			EscapeButton ();
			Debug.Log("Collecnt");
			GC.Collect();
		}
			
		if(Input.GetKeyDown("joystick button 0")){
			EscapeButton ();
		}
	}
		


	private void ReadPCInput() {
		if (Input.GetKeyDown (KeyCode.W)) {
			MenuButtonUp ();
		}

		if (Input.GetKeyDown (KeyCode.S)) {
			MenuButtonDown ();
		}

		if (Input.GetKeyDown (KeyCode.A)) {
			MenuButtonLeft ();
		}

		if (Input.GetKeyDown (KeyCode.D)) {
			MenuButtonRight ();
		}

		if (Input.GetKeyDown (KeyCode.Escape)) {
			EscapeButton ();
		}

		if (Input.GetKeyDown (KeyCode.Space)) {
			ConfirmButton ();
		}
	}
}
