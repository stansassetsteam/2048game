using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ST_BillingManager : Singletone<ST_BillingManager> {
	private static bool _IsBillingInitialized =  false;

	public static event Action SuccesPurchaseEvent = delegate {};
	
	//--------------------------------------
	// INITIALIZE
	//--------------------------------------

	void Awake() {
		DontDestroyOnLoad(gameObject);

		UM_InAppPurchaseManager.Client.Connect ();
		UM_InAppPurchaseManager.Client.OnServiceConnected += OnBillingConnectFinishedAction;
		UM_InAppPurchaseManager.Client.OnPurchaseFinished += OnPurchaseFlowFinishedAction;

		//UM_InAppPurchaseManager.OnBillingConnectFinishedAction += OnBillingConnectFinishedAction;
		//UM_InAppPurchaseManager.OnPurchaseFlowFinishedAction += OnPurchaseFlowFinishedAction;
	}
	
	//--------------------------------------
	//  PUBLIC METHODS
	//--------------------------------------

	public void Init() {
		//Just to init billing in the garage scene.
	}

	public void RestorePurchases() {
		UM_InAppPurchaseManager.Instance.RestorePurchases();
	}

	public void Purchase(PurchaseProducts product) {
		UM_InAppPurchaseManager.instance.Purchase(product.ToString());
	}

	public bool IsPurchased(PurchaseProducts product) {
		return UM_InAppPurchaseManager.instance.IsProductPurchased(product.ToString());
	}

	public string GetProductPrice(PurchaseProducts product) {
		UM_InAppProduct p = UM_InAppPurchaseManager.GetProductById(product.ToString());
		if(p != null) {
			if(p.LocalizedPrice == null) {
				return "---";
			} else {
				return p.LocalizedPrice;
			}
		} else {
			return "---";
		}
	}

	//--------------------------------------
	//  GET/SET
	//--------------------------------------

	public static bool IsBillingInitialized {
		get {
			return _IsBillingInitialized;
		}
	}

	//--------------------------------------
	//  EVENTS
	//--------------------------------------


	private void OnBillingConnectFinishedAction (UM_BillingConnectionResult result) {
		UM_InAppPurchaseManager.Client.OnServiceConnected -= OnBillingConnectFinishedAction;

		if(result.isSuccess) {
			_IsBillingInitialized = true;

			ChreckRemoveAdPurchased ();
		}
	}

	public void ChreckRemoveAdPurchased() {
		if (ST_BillingManager.Instance.IsPurchased (PurchaseProducts.remove_ads)) {
			PlayerData.Instance.AdsBLockedByPurchase = true;
			GameMenuController.Instance.HideBannerAd ();
		}
	}

	private void OnPurchaseFlowFinishedAction (UM_PurchaseResult result) {
		if(result.isSuccess) {			
			PlayerData.Instance.AdsBLockedByPurchase = true;
			GameMenuController.Instance.HideBannerAd ();
			SuccesPurchaseEvent ();

		} else  {
			switch(Application.platform) {
			case RuntimePlatform.Android:

				if(result.ResponceCode != BillingResponseCodes.BILLINGHELPERR_USER_CANCELLED) {
					Debug.Log("purchase_badly");
				}
				break;
			case RuntimePlatform.IPhonePlayer:
				Debug.Log(result.IOS_PurchaseInfo.Receipt);
				break;
			}
		}
	}

	//--------------------------------------
	//  PRIVATE METHODS
	//--------------------------------------

	//--------------------------------------
	//  DESTROY
	//--------------------------------------	

}
