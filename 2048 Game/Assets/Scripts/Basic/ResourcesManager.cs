﻿using UnityEngine;
using System.Collections;
using System.IO;

public static class ResourcesManager  {
	private const string RESOURCES_PREFABS_PATH = "Prefabs/";
	private const string FILLED_CELL = "FilledCell";

	public static FilledCell LoadNod() {
		GameObject prefab =  Object.Instantiate (Resources.Load (RESOURCES_PREFABS_PATH + FILLED_CELL)) as GameObject;

		return prefab.GetComponent<FilledCell>();
	}

	public static GameObject LoadObjectPrefab(string orafyb) {
		GameObject prefab =  Object.Instantiate (Resources.Load (RESOURCES_PREFABS_PATH + orafyb)) as GameObject;

		return prefab;
	}
}
