using UnityEngine;
using System.Collections;

public abstract class Singletone<T> : MonoBehaviour where T : MonoBehaviour {
	private static T _Instance = null;

	public static T Instance {
		get {
			if (_Instance == null) {
				_Instance = GameObject.FindObjectOfType(typeof(T)) as T;
				if (_Instance == null) {
					_Instance = new GameObject (typeof(T).Name).AddComponent<T> ();
				}
			}

			return _Instance;
		}
	}

	public static bool HasInstance {
		get {
			if (_Instance == null) {
				return false;
			} else {
				return true;
			}
		}
	}
}
