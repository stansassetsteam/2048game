using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlurryUnity_UseExamples : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
		FlurryUnity.Instance.Init();


		string eventId = "MyEvent";
		FlurryUnity.Instance.LogEvent(eventId);
	

		Dictionary<string, string> eventData =  new Dictionary<string, string>();
		eventData.Add("Age", "16");
		eventData.Add("Gender", "m");
		FlurryUnity.Instance.LogEvent(eventId, eventData);



		FlurryUnity.Instance.OnError("ERROR_NAME", "ERROR_MESSAGE", "ERROR_CLASS");



		FlurryUnity.Instance.OnPageView();

	
		FlurryUnity.Instance.SetAge(16);
		FlurryUnity.Instance.SetGender(FA_Gender.Male);



	}
	

}
