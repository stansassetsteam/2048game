using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class FlurryUnity : SA.Common.Pattern.Singleton<FlurryUnity> {

	private iFlurryUnity _Proxy = null;


	void Awake() {
		switch(Application.platform) {
		case RuntimePlatform.Android:
			_Proxy =  new FA_Android();
			break;

		case RuntimePlatform.IPhonePlayer:
			_Proxy =  new FA_IOS();
			break;
		default:
			_Proxy = new FA_Editor();
			break;
		}

	}

	void OnApplicationPause(bool IsPaused) {
		if(IsPaused) {
			_Proxy.OnEndSession();
		} else {
			_Proxy.OnStartSession();
		}
	}

	

	public void Init() {

		SetLogEnabled(FA_Settings.Instance.EnableFlurryLog);

		_Proxy.Init();

		SetCrashReporting(FA_Settings.Instance.CrashReporting);
		SetReportLocation(FA_Settings.Instance.ReportLocation);


	}

	public void LogEvent(string eventId) {
		_Proxy.LogEvent(eventId);
	}

	public void LogEvent(string eventId, Dictionary<string, string> parameters) {
		_Proxy.LogEvent(eventId, parameters);
	}
	
	public void OnError(string errorId, string message, string errorClass = "") {
		_Proxy.OnError(errorId, message, errorClass);
	}
	
	public void OnPageView() {
		_Proxy.OnPageView();
	}

	public void SetUserID(string userId) {
		_Proxy.SetUserID(userId);
	}

	public void SetAge(int age) {
		_Proxy.SetAge(age);
	}

	public void SetGender(FA_Gender gender) {
		_Proxy.SetGender(gender);
	}



	public void SetReportLocation(bool reportLocation) {
		_Proxy.SetReportLocation(reportLocation);
	}

	public void SetCrashReporting(bool enabled) {
		_Proxy.SetCrashReporting(enabled);
	}


	public void SetLogEnabled(bool enabled) {
		_Proxy.SetLogEnabled(enabled);
	}







	
}
