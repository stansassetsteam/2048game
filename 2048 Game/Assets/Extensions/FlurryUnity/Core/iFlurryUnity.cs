using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface iFlurryUnity {

	void Init();

	void OnStartSession();
	void OnEndSession();

	void LogEvent(string eventId);
	void LogEvent(string eventId, Dictionary<string, string> parameters);

	void OnError(string errorId, string message, string errorClass);

	void OnPageView();
	void SetUserID(string userId);
	void SetAge(int age);
	void SetGender(FA_Gender gender);

	void SetReportLocation(bool reportLocation);
	void SetCrashReporting(bool enabled);
	void SetLogEnabled(bool enabled);
}
