﻿using UnityEngine;
using System.IO;
using System.Collections.Generic;


#if UNITY_EDITOR
using UnityEditor;
[InitializeOnLoad]
#endif

public class FA_Settings : ScriptableObject {

	public const string VERSION_NUMBER = "1.0";




	public string IOS_ApplicationKey = "IOS_APP_KEY";
	public string Android_ApplicationKey = "ANDROID_APP_KEY";
	public bool ShowAppPermissions = false;

	public bool ReportLocation = false;
	public bool CrashReporting = false;
	public bool EnableFlurryLog = false;


	private const string FAssetPath = "Extensions/FlurryUnity/Resources";
	private const string FAssetName = "FlurrySettingsResource";
	private const string FAssetExtension = ".asset";
	

	private static FA_Settings _instance;
	
	public static FA_Settings Instance {
		get {
			if(_instance == null) {
				_instance = Resources.Load(FAssetName) as FA_Settings;
				if(_instance == null)
				{
					_instance = CreateInstance<FA_Settings>();
					#if UNITY_EDITOR
					
					
					SA.Common.Util.Files.CreateFolder(FAssetPath);
					
					string fullPath = Path.Combine(Path.Combine("Assets", FAssetPath), FAssetName + FAssetExtension );
					
					AssetDatabase.CreateAsset(_instance, fullPath);
					#endif
					
				}
			}
			return _instance;
		}
	}
}
