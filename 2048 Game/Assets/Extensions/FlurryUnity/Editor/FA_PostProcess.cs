﻿using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Collections;

public class FA_PostProcess : MonoBehaviour {

	#if UNITY_IPHONE
	[PostProcessBuild(47)]
	public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject) {
		

		string SecurityFramework = "Security.framework";
		if(!ISDSettings.Instance.frameworks.Contains(SecurityFramework)) {
			ISDSettings.Instance.frameworks.Add(SecurityFramework);
		}

		Debug.Log("FA Postprocess Done");
		
	}
	#endif
}
