using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System.Reflection;


[CustomEditor(typeof(FA_Settings))]
public class FA_EditorWindow : Editor {

	GUIContent SdkVersion   = new GUIContent("Plugin Version [?]", "This is Plugin version.  If you have problems or compliments please include this so we know exactly what version to look out for.");
	GUIContent SupportEmail = new GUIContent("Support [?]", "If you have any technical quastion, feel free to drop an e-mail");
	

	void Awake() {
		UpdateManifest();
	}
	
	
	public override void OnInspectorGUI () {
		
		
		GUI.changed = false;
		
		
		EditorGUILayout.LabelField("Flurry Settings", EditorStyles.boldLabel);
		EditorGUILayout.Space();
		
		General();
		EditorGUILayout.Space();
		AboutGUI();
		
		if(GUI.changed) {
			DirtyEditor();
			UpdateManifest();
		}
		
	}


	private void General() {
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("IOS Application Key");
		FA_Settings.Instance.IOS_ApplicationKey = EditorGUILayout.TextField(FA_Settings.Instance.IOS_ApplicationKey, GUILayout.Width(200));
		EditorGUILayout.EndHorizontal();

		//EditorGUILayout.Space();
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Android Application Key");
		FA_Settings.Instance.Android_ApplicationKey = EditorGUILayout.TextField(FA_Settings.Instance.Android_ApplicationKey, GUILayout.Width(200));
		EditorGUILayout.EndHorizontal();


		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Report Location");
		FA_Settings.Instance.ReportLocation = EditorGUILayout.Toggle(FA_Settings.Instance.ReportLocation);
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Crash Reporting");
		FA_Settings.Instance.CrashReporting = EditorGUILayout.Toggle(FA_Settings.Instance.CrashReporting);
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("Enable Flurry Logs");
		FA_Settings.Instance.EnableFlurryLog = EditorGUILayout.Toggle(FA_Settings.Instance.EnableFlurryLog);
		EditorGUILayout.EndHorizontal();





	
		FA_Settings.Instance.ShowAppPermissions = EditorGUILayout.Foldout(FA_Settings.Instance.ShowAppPermissions, "Android Application Permissions");
		if(FA_Settings.Instance.ShowAppPermissions) {
			SA.Manifest.Manager.Refresh();

			EditorGUILayout.LabelField("Required By Flurry Plugin:", EditorStyles.boldLabel);
			List<string> permissions = GetRequiredPermissions();
			
			foreach(string p in permissions) {
				EditorGUILayout.BeginVertical (GUI.skin.box);
				EditorGUILayout.BeginHorizontal();
				
				EditorGUILayout.SelectableLabel(p, GUILayout.Height(16));
				
				EditorGUILayout.EndHorizontal();
				EditorGUILayout.EndVertical();
			}
			
			EditorGUILayout.Space();
			
			EditorGUILayout.LabelField("Other Permissions in Manifest:", EditorStyles.boldLabel);
			foreach(SA.Manifest.PropertyTemplate tpl in SA.Manifest.Manager.GetManifest().Permissions) {
				if(!permissions.Contains(tpl.Name)) {
					
					EditorGUILayout.BeginVertical (GUI.skin.box);
					EditorGUILayout.BeginHorizontal();
					
					EditorGUILayout.SelectableLabel(tpl.Name, GUILayout.Height(16));
					if(GUILayout.Button("x",  GUILayout.Width(20))) {
                        SA.Manifest.Manager.GetManifest().RemovePermission(tpl);
                        SA.Manifest.Manager.SaveManifest();
						return;
					}
					
					EditorGUILayout.EndHorizontal();
					EditorGUILayout.EndVertical();
				}
			} 

		}

	}


	private void AboutGUI() {
		EditorGUILayout.HelpBox("About the Plugin", MessageType.None);
		EditorGUILayout.Space();
		
		SelectableLabelField(SdkVersion, FA_Settings.VERSION_NUMBER);
		SelectableLabelField(SupportEmail, "stans.assets@gmail.com");
	}


	public static void UpdateManifest() {


        SA.Manifest.Manager.Refresh();
		SA.Manifest.Template Manifest = SA.Manifest.Manager.GetManifest();


		
		////////////////////////
		//Permissions 
		////////////////////////
		
		List<string> permissions = GetRequiredPermissions();
		foreach(string p in permissions) {
			Manifest.AddPermission(p);
		}

        SA.Manifest.Manager.SaveManifest();
	}




	
	private void SelectableLabelField(GUIContent label, string value) {
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField(label, GUILayout.Width(180), GUILayout.Height(16));
		EditorGUILayout.SelectableLabel(value, GUILayout.Height(16));
		EditorGUILayout.EndHorizontal();
	}



	private static List<string> GetRequiredPermissions() {
		List<string> permissions =  new List<string>();
		permissions.Add("android.permission.INTERNET");
		permissions.Add("android.permission.ACCESS_NETWORK_STATE");
		permissions.Add("android.permission.ACCESS_FINE_LOCATION");

		return permissions;
	}
	
	private static void DirtyEditor() {
		#if UNITY_EDITOR
		EditorUtility.SetDirty(FA_Settings.Instance);
		#endif
	}
}
