using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;



public class FA_Android : iFlurryUnity {


	#if UNITY_ANDROID && !UNITY_EDITOR

	private static AndroidJavaClass  cls_FlurryAgent = new AndroidJavaClass("com.flurry.android.FlurryAgent");
	private static AndroidJavaClass cls_FlurryAgentConstants = new AndroidJavaClass("com.flurry.android.Constants");

	private static readonly string UnityPlayerClassName = "com.unity3d.player.UnityPlayer";
	private static readonly string UnityPlayerActivityName = "currentActivity";

	#endif


	public void Init() {
		#if UNITY_ANDROID && !UNITY_EDITOR
		Debug.Log("FA Init");
		using (AndroidJavaClass unityPlayer = new AndroidJavaClass(UnityPlayerClassName)) {
			using (AndroidJavaObject activity = unityPlayer.GetStatic<AndroidJavaObject>(UnityPlayerActivityName)) {
				cls_FlurryAgent.CallStatic("init", activity, FA_Settings.Instance.Android_ApplicationKey);
			}
		}
		#endif
	}

	
	public void OnStartSession() {
		#if UNITY_ANDROID && !UNITY_EDITOR
		Debug.Log("FA OnStartSession");
		using(AndroidJavaClass cls_UnityPlayer = new AndroidJavaClass(UnityPlayerClassName)) {
			using(AndroidJavaObject obj_Activity = cls_UnityPlayer.GetStatic<AndroidJavaObject>(UnityPlayerActivityName)) {
				cls_FlurryAgent.CallStatic("onStartSession", obj_Activity, FA_Settings.Instance.Android_ApplicationKey);
			}
		}
		#endif
	}

	
	public void OnEndSession() {
		#if UNITY_ANDROID && !UNITY_EDITOR
		Debug.Log("FA OnEndSession");
		using(AndroidJavaClass cls_UnityPlayer = new AndroidJavaClass(UnityPlayerClassName)) {
			using(AndroidJavaObject obj_Activity = cls_UnityPlayer.GetStatic<AndroidJavaObject>(UnityPlayerActivityName)) {
				cls_FlurryAgent.CallStatic("onEndSession", obj_Activity);
			}
		}
		#endif
	}


	
	public void OnError(string errorId, string message, string errorClass) {
		#if UNITY_ANDROID && !UNITY_EDITOR
		cls_FlurryAgent.CallStatic("onError", errorId, message, errorClass);
		#endif
	}
	
	public void OnPageView() {
		#if UNITY_ANDROID && !UNITY_EDITOR
		cls_FlurryAgent.CallStatic("onPageView");
		#endif
	}
	
	public void SetUserID(string userId) {
		#if UNITY_ANDROID && !UNITY_EDITOR
		cls_FlurryAgent.CallStatic("setUserID", userId);
		#endif
	}
	
	public void SetAge(int age) {
		#if UNITY_ANDROID && !UNITY_EDITOR
		cls_FlurryAgent.CallStatic("setAge", age);
		#endif
	}
	
	public void SetGender(FA_Gender gender) {
		#if UNITY_ANDROID && !UNITY_EDITOR
		byte javaGender = (gender == FA_Gender.Male ? cls_FlurryAgentConstants.GetStatic<byte>("MALE") : cls_FlurryAgentConstants.GetStatic<byte>("FEMALE"));
		cls_FlurryAgent.CallStatic("setGender", javaGender);
		#endif
	}
	
	public void SetReportLocation(bool reportLocation) {
		#if UNITY_ANDROID && !UNITY_EDITOR
		cls_FlurryAgent.CallStatic("setReportLocation", reportLocation);
		#endif
	}
	
	public void SetCrashReporting(bool enabled) {
		#if UNITY_ANDROID && !UNITY_EDITOR
		cls_FlurryAgent.CallStatic("setCaptureUncaughtExceptions", enabled);
		#endif
	}
	

	public void SetLogEnabled(bool enabled) {
		#if UNITY_ANDROID && !UNITY_EDITOR
		cls_FlurryAgent.CallStatic("setLogEnabled", enabled);
		#endif
	}


	public void LogEvent(string eventId) {
		#if UNITY_ANDROID && !UNITY_EDITOR
		cls_FlurryAgent.CallStatic("logEvent", eventId);
		#endif
	}
	
	public void LogEvent(string eventId, Dictionary<string, string> parameters) {
		#if UNITY_ANDROID && !UNITY_EDITOR
		Hashtable hashParams = new Hashtable(parameters);

		using(AndroidJavaObject obj_HashMap = new AndroidJavaObject("java.util.HashMap"))
		{
			// Call 'put' via the JNI instead of using helper classes to avoid:
			//  "JNI: Init'd AndroidJavaObject with null ptr!"
			IntPtr method_Put = AndroidJNIHelper.GetMethodID(obj_HashMap.GetRawClass(), "put",
			                                                 "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");
			
			object[] args = new object[2];
			foreach (DictionaryEntry kvp in hashParams)
			{
				using(AndroidJavaObject k = new AndroidJavaObject("java.lang.String", kvp.Key + ""))
				{
					using(AndroidJavaObject v = new AndroidJavaObject("java.lang.String", kvp.Value + ""))
					{
						args[0] = k;
						args[1] = v;
						AndroidJNI.CallObjectMethod(obj_HashMap.GetRawObject(),
						                            method_Put, AndroidJNIHelper.CreateJNIArgArray(args));
					}
				}
			}
			cls_FlurryAgent.CallStatic("logEvent", eventId, obj_HashMap);
		}
		#endif
	}

}
