﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FA_Editor : iFlurryUnity {

	public void Init() {

	}
	
	public void OnStartSession() {

	}
	public void OnEndSession() {

	}
	
	public void LogEvent(string eventId) {

	}
	public void LogEvent(string eventId, Dictionary<string, string> parameters) {

	}
	
	public void OnError(string errorId, string message, string errorClass) {

	}
	
	public void OnPageView() {

	}
	public void SetUserID(string userId) {

	}

	public void SetAge(int age) {

	}
	public void SetGender(FA_Gender gender) {

	}
	
	public void SetReportLocation(bool reportLocation) {

	}
	public void SetCrashReporting(bool enabled) {

	}
	public void SetLogEnabled(bool enabled) {

	}
}
