#define SA_DEBUG_MODE
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if (UNITY_IPHONE && !UNITY_EDITOR) || SA_DEBUG_MODE
using System.Runtime.InteropServices;
#endif

public class FA_IOS : iFlurryUnity {


	#if (UNITY_IPHONE && !UNITY_EDITOR) || SA_DEBUG_MODE


	[DllImport ("__Internal")]
	private static extern void _SA_FA_Init(string apiKey);
	
	[DllImport ("__Internal")]
	private static extern void _SA_FA_LogEvent(string eventName);
	
	[DllImport ("__Internal")]
	private static extern void _SA_FA_LogEventWithParams(string eventName, string dataIds, string dataValues);
	
	[DllImport ("__Internal")]
	private static extern void _SA_FA_SetUserId(string value);
	
	[DllImport ("__Internal")]
	private static extern void _SA_FA_SetUserAge(int value);
	
	[DllImport ("__Internal")]
	private static extern void _SA_FA_SetUserGender(string value);
	
	[DllImport ("__Internal")]
	private static extern void _SA_FA_LogError(string errorId, string message);
	
	[DllImport ("__Internal")]
	private static extern void _SA_FA_LogPageView();
	
	[DllImport ("__Internal")]
	private static extern void _SA_FA_SetLogEnabled(bool enabled);

	
	#endif
	
	public void Init() {
		#if (UNITY_IPHONE && !UNITY_EDITOR) || SA_DEBUG_MODE
		_SA_FA_Init(FA_Settings.Instance.IOS_ApplicationKey);
		#endif
	}
	
	
	public void OnStartSession() {
		//Not supported by ios implementation
	}
	
	
	public void OnEndSession() {
		//Not supported by ios implementation
	}
	
	
	public void OnError(string errorId, string message, string errorClass = "") {
		#if (UNITY_IPHONE && !UNITY_EDITOR) || SA_DEBUG_MODE
		_SA_FA_LogError(errorId, message);
		#endif
	}
	
	public void OnPageView() {
		#if (UNITY_IPHONE && !UNITY_EDITOR) || SA_DEBUG_MODE
		_SA_FA_LogPageView();
		#endif
	}
	
	public void SetUserID(string userId) {
		#if (UNITY_IPHONE && !UNITY_EDITOR) || SA_DEBUG_MODE
		_SA_FA_SetUserId(userId);
		#endif
	}
	
	public void SetAge(int age) {
		#if (UNITY_IPHONE && !UNITY_EDITOR) || SA_DEBUG_MODE
		_SA_FA_SetUserAge(age);
		#endif
	}
	
	public void SetGender(FA_Gender gender) {
		#if (UNITY_IPHONE && !UNITY_EDITOR) || SA_DEBUG_MODE
		if(gender == FA_Gender.Male) {
			_SA_FA_SetUserGender("m");
		} else {
			_SA_FA_SetUserGender("f");
		}

		#endif
	}
	
	public void SetReportLocation(bool reportLocation) {
		//Not supported by ios implementation
	}
	
	public void SetCrashReporting(bool enabled) {
		//Not supported by ios implementation
	}
	

	public void LogEvent(string eventId) {
		#if (UNITY_IPHONE && !UNITY_EDITOR) || SA_DEBUG_MODE
		_SA_FA_LogEvent(eventId);
		#endif
	}
	
	public void LogEvent(string eventId, Dictionary<string, string> parameters) {
		#if (UNITY_IPHONE && !UNITY_EDITOR) || SA_DEBUG_MODE

		List<string> ids =  new List<string>();
		List<string> values =  new List<string>();
		
		foreach( KeyValuePair<string, string> pair in parameters) {
			ids.Add(pair.Key);
			values.Add(pair.Value);
		}

		string idsString = SerializeArray(ids.ToArray());
		string valuesString = SerializeArray(values.ToArray());
		
		
		_SA_FA_LogEventWithParams(eventId, idsString, valuesString);


		#endif
	}


	public void SetLogEnabled(bool enabled) {
		#if (UNITY_IPHONE && !UNITY_EDITOR) || SA_DEBUG_MODE
		_SA_FA_SetLogEnabled(enabled);
		#endif
	}

	public const char DATA_SPLITTER = '|';
	public static string SerializeArray(string[] array) {
		
		if(array == null) {
			return string.Empty;
		} else {
			if(array.Length == 0) {
				return string.Empty;
			} else {
				
				string serializedArray = "";
				int len = array.Length;
				for(int i = 0; i < len; i++) {
					if(i != 0) {
						serializedArray += DATA_SPLITTER;
					}
					
					serializedArray += array[i];
				}
				
				return serializedArray;
			}
		}
	}
}
