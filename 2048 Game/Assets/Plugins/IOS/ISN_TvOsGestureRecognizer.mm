


#import <Foundation/Foundation.h>
#import "ISN_NativeCore.h"


NSString * const UNITY_SPLITTER = @"|";
NSString * const UNITY_EOF = @"endofline";



@interface TvOsGestureRecognizer : NSObject

+ (TvOsGestureRecognizer *)sharedInstance;

-(void) Start;

@end



static TvOsGestureRecognizer * gesture_sharedInstance;



@implementation TvOsGestureRecognizer


+ (id)sharedInstance {
    if (gesture_sharedInstance == nil)  {
        gesture_sharedInstance = [[self alloc] init];
    }
    
    return gesture_sharedInstance;
}


- (void)Start {
    
    UISwipeGestureRecognizer * swipe;
    UIViewController *vc =  UnityGetGLViewController();
    
    swipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(SwipeAction:)];
    swipe.direction=UISwipeGestureRecognizerDirectionRight;
    [vc.view addGestureRecognizer:swipe];
    
    swipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(SwipeAction:)];
    swipe.direction=UISwipeGestureRecognizerDirectionLeft;
    [vc.view addGestureRecognizer:swipe];
    
    swipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(SwipeAction:)];
    swipe.direction=UISwipeGestureRecognizerDirectionUp;
    [vc.view addGestureRecognizer:swipe];
    
    swipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(SwipeAction:)];
    swipe.direction=UISwipeGestureRecognizerDirectionDown;
    [vc.view addGestureRecognizer:swipe];
    
}

-(void)SwipeAction:(UISwipeGestureRecognizer*)gestureRecognizer {
    //Do what you want here
    
    if(gestureRecognizer.direction == UISwipeGestureRecognizerDirectionUp) {
        UnitySendMessage("TvOsGestureRecognizer", "OnSwipe", "0");
    }
    
    if(gestureRecognizer.direction == UISwipeGestureRecognizerDirectionDown) {
        UnitySendMessage("TvOsGestureRecognizer", "OnSwipe", "1");
    }
    
    if(gestureRecognizer.direction == UISwipeGestureRecognizerDirectionLeft) {
        UnitySendMessage("TvOsGestureRecognizer", "OnSwipe", "2");
    }
    
    if(gestureRecognizer.direction == UISwipeGestureRecognizerDirectionRight) {
        UnitySendMessage("TvOsGestureRecognizer", "OnSwipe", "3");
    }
    
    NSLog(@"direction: %lu",  (unsigned long)gestureRecognizer.direction );
    
}


@end



extern "C" {
    
    
    
    void _ISN_InitTvOsGestureRecognizer() {
        [[TvOsGestureRecognizer sharedInstance] Start];
    }
    
}






