
 #import "Flurry.h"


@interface FA_Analytics : NSObject

+ (id) sharedInstance;
+ (NSArray *) charToNSArray:(char *)value;
+ (NSString *) charToNSString:(char *)value;


- (void) init:(NSString*)apiKey;

- (void) logEvent:(NSString *)eventName eventData: (NSString *) eventData;
- (void) logError:(NSString *)errorId message: (NSString *) message;
- (void) logPageView;


- (void) setUserId:(NSString *)value;
- (void) setUserAge:(int)value;
- (void) setUserGender:(NSString *)value;

- (void) setLogEnabled:(BOOL) value;



@end


@implementation FA_Analytics

static FA_Analytics * fa_sharedInstance;

+ (id)sharedInstance {
    
    if (fa_sharedInstance == nil)  {
        fa_sharedInstance = [[self alloc] init];
    }
    
    return fa_sharedInstance;
}

-(void) init:(NSString *)apiKey {
    [Flurry startSession:apiKey];
}



- (void) logEvent:(NSString *)eventName eventData: (NSDictionary *) eventData {
    if(eventData.count > 0) {
        [Flurry logEvent:eventName];
    } else {
        [Flurry logEvent:eventName withParameters:eventData];
    }
    
}

- (void) logError:(NSString *)errorId message:(NSString *)message {
    [Flurry logError:errorId message:message exception:NULL];
}

- (void) logPageView {
    [Flurry logPageView];
}

-(void) setLogEnabled:(BOOL)value {
    if(value) {
        [Flurry setLogLevel:FlurryLogLevelAll];
    } else {
        [Flurry setLogLevel:FlurryLogLevelNone];
    }
}



-(void) setUserId:(NSString *)value {
    [Flurry setUserID:value];
}

- (void) setUserAge:(int)value {
    [Flurry setAge:value];
}

- (void) setUserGender:(NSString *)value {
    [Flurry setGender:value];
}



+(NSString *) charToNSString:(char *)value {
    if (value != NULL) {
        return [NSString stringWithUTF8String: value];
    } else {
        return [NSString stringWithUTF8String: ""];
    }
}


+ (NSArray *)charToNSArray:(char *)value {
    NSString* strValue = [FA_Analytics charToNSString:value];
    
    NSArray *array;
    if([strValue length] == 0) {
        array = [[NSArray alloc] init];
    } else {
        array = [strValue componentsSeparatedByString:@"|"];
    }
    
    return array;
}


@end

extern "C" {
    
    
    //--------------------------------------
    //  Flurry
    //--------------------------------------
    
    void _SA_FA_Init(char* apiKey) {
        [[FA_Analytics sharedInstance] init: [FA_Analytics charToNSString:apiKey]];
       
    }
  

    
    void _SA_FA_LogEvent(char* event) {
        NSDictionary* dic =  [[NSDictionary alloc] init];
        [[FA_Analytics sharedInstance] logEvent:[FA_Analytics charToNSString:event] eventData: dic];
    }
    
    void _SA_FA_LogEventWithParams(char* event, char* dataIds, char* dataValues) {
        
        NSArray* ids = [FA_Analytics charToNSArray:dataIds];
        NSArray* values = [FA_Analytics charToNSArray:dataValues];
        NSMutableDictionary* dic =  [[NSMutableDictionary alloc] init];

        
        for (int i=0; i< [ids count]; i++){
            [dic setValue:values[i] forKey:ids[i]];
        }
        
        
        [[FA_Analytics sharedInstance] logEvent:[FA_Analytics charToNSString:event] eventData: dic];
    }
    
    
    
    void _SA_FA_SetUserId(char* value) {
        [[FA_Analytics sharedInstance] setUserId:[FA_Analytics charToNSString:value]];
    }
    
    void _SA_FA_SetUserAge(int value) {
        [[FA_Analytics sharedInstance] setUserAge:value];
    }
    
    void _SA_FA_SetUserGender(char* value) {
        [[FA_Analytics sharedInstance] setUserGender:[FA_Analytics charToNSString:value]];
    }
    
    void _SA_FA_LogError(char* errorId, char* message) {
        [[FA_Analytics sharedInstance] logError:[FA_Analytics charToNSString:errorId] message:[FA_Analytics charToNSString:message]];
    }
    
    void _SA_FA_LogPageView() {
        [[FA_Analytics sharedInstance] logPageView];
    }
    
    void _SA_FA_SetLogEnabled(BOOL enabled) {
        [[FA_Analytics sharedInstance] setLogEnabled:enabled];
    }
    

}