﻿#if UNITY_IOS

using System;
using UnityEngine;


namespace Jukko.iOS
{
	/// <summary>
	/// Jukko iOS SDK.
	/// </summary>
	public class JukkoIOSSdk : JukkoSdkInterface
	{
		public  JukkoIOSSdk ()
		{
		}

		public void Init (string apiKey)
		{
			Externs.initializeJukko (apiKey);
		}

		public bool DebugMode {
			get
			{
				return Externs.getDebugMode();
			}
			set
			{
				Externs.setDebugMode(value);
			}
		}


		public int AdsFrequency { 
			
			get
			{
				return Externs.getAdsFrequency();
			}

			set
			{
				Externs.setAdsFrequency(value);
			}
		}

		public void ShowAd (AdClosed callback)
		{		
			onAdClosed = callback;
			Externs.showAd ();
		}

		#region iOS-specific 

		private static event AdClosed onAdClosed;

		internal static void triggerEvent(AdClosedEvent completionData)
		{
			if (onAdClosed != null) {					
				onAdClosed.Invoke (completionData);
			}
		}

		#endregion
	}
}
#endif
