﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using SimpleJSON;
using UnityEngine;

namespace Jukko.iOS
{

	internal static class JsonUtils
	{

		internal static AdClosedEvent jsonToAdEvent(string jsonString)
		{
			var adEvent = new AdClosedEvent ();


			var N = JSON.Parse(jsonString);

			adEvent.Reason = StringToReason (N ["reason"]);
			adEvent.Message = N ["message"];
			adEvent.Events = new List<EventInfo> ();

			var eventsJsonArray = N["events"].AsArray;  

			foreach (JSONNode item in eventsJsonArray)
			{
				var singleEvent = StringToAdEvent (item ["adEvent"]);
				var timestamp = StringToDateTime (item ["timestamp"]);
				EventInfo advertEvent = new EventInfo (singleEvent, timestamp);
				adEvent.Events.Add (advertEvent);
			}

			return adEvent;
		}
			
		private static Reason StringToReason(string value) 
		{
			switch (value) {
			case "error":
				return Reason.Error;

			case "userClosed":
				return Reason.ClosedByUser;

			case "timeout":
				return Reason.Timeout;

			case "frequencyCapping":
				return Reason.FrequencyCapping;

			case "networkConnectivity":
				return Reason.NetworkConnectivity;

			default:
				return Reason.Error;

			}
		}

		private static DateTime StringToDateTime(string timestamp)
		{
			DateTime dt = DateTime.ParseExact(timestamp, "yyyy-MM-ddTHH:mm:sszzzz", CultureInfo.InvariantCulture);
			return dt;
		}

		private static AdEvent StringToAdEvent(string adEvent)
		{
			switch (adEvent) {
			case "advert":
				return AdEvent.AdShown;

			case "adUrlOpened":
				return AdEvent.AdUrlOpened;

			case "intro":
				return AdEvent.IntroShown;

			case "outro":
				return AdEvent.OutroShown;

			case "progress":
				return AdEvent.ProgressShown;

			case "close":
				return AdEvent.Close;

			case "launch":
				return AdEvent.Launch;
			}

			throw new KeyNotFoundException (string.Format ("AdEvent with name '{0}' does not exist", adEvent));
		}
	}
}

