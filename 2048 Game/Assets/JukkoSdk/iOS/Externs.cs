﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using AOT;

#if UNITY_IOS

namespace Jukko.iOS
{
	internal class Externs : MonoBehaviour
	{
		[DllImport ("__Internal")]
		internal static extern void initializeJukko (string apiKey);

		[DllImport ("__Internal")]
		private static extern void showAd (JukkoShowAdCallback callback);

		[DllImport ("__Internal")]
		internal static extern void setAdsFrequency (int interval);

		[DllImport ("__Internal")]
		internal static extern int getAdsFrequency ();

		[DllImport ("__Internal")]
		internal static extern void setDebugMode (bool mode);

		[DllImport ("__Internal")]
		internal static extern bool getDebugMode ();

		internal delegate void JukkoShowAdCallback(string jsonString);

		[MonoPInvokeCallback (typeof(JukkoShowAdCallback))]
		internal static void jukkoCallbackReceived (string jsonString)
		{
			if (Application.platform == RuntimePlatform.IPhonePlayer) {

				Debug.Log ("ShowAd callback is returned from Jukko SDK");

				var adClosedEvent = JsonUtils.jsonToAdEvent (jsonString);
				JukkoIOSSdk.triggerEvent (adClosedEvent);
			}
		}

		public static void showAd()
		{
			showAd (jukkoCallbackReceived);
		}

	}	
}
#endif
