﻿using System;
using UnityEngine;

namespace Jukko
{
	public class JukkoDummySdk : JukkoSdkInterface
	{
		public JukkoDummySdk ()
		{
			Debug.Log ("Current platform is not supported.");
		}

		public void Init (string apiKey)
		{
			Debug.Log ("Dummy Init");
		}

		public bool DebugMode {
			get {
				Debug.Log ("Dummy get DebugMode");
				return false;
			}
			set { Debug.Log ("Dummy set DebugMode"); }
		}

		public int AdsFrequency {
			get {
				Debug.Log ("Dummy get AdsFrequency");
				return -1;
			}
			set { Debug.Log ("Dummy set AdsFrequency"); }
		}

		public void ShowAd (AdClosed callback)
		{
			Debug.Log ("Dummy ShowAd");
		}

	}
}
