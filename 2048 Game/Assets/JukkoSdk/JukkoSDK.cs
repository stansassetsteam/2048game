﻿using System;
using System.Collections.Generic;

namespace Jukko
{
	public class JukkoSdk
	{
		private static JukkoSdkInterface instance;

		/// <summary>
		/// Gets the instance of Jukko SDK.
		/// </summary>
		/// <value>The instance.</value>
		public static JukkoSdkInterface Instance {
			get {
				if (instance == null) {
					#if UNITY_ANDROID
					instance = new Android.JukkoAndroidSdk ();
					#elif UNITY_IOS
					instance = new iOS.JukkoIOSSdk();
					#else
					instance = new JukkoDummySdk ();
					#endif
				}
				return instance;
			}
		}

		private JukkoSdk ()
		{
		}

	}

	/// <summary>
	/// Jukko SDK interface.
	/// </summary>
	public interface JukkoSdkInterface
	{
		/// <summary>
		/// Initialize Jukko SDK.
		/// </summary>
		/// <param name="apiKey">API key.</param>
		void Init (string apiKey);

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="JukkoSdk.JukkoSdkInterfaceV2"/> is in debug mode.
		/// </summary>
		/// <value><c>true</c> if debug mode enabled; otherwise, <c>false</c>.</value>
		bool DebugMode { get; set; }

		/// <summary>
		/// Gets or sets the ads frequency (in seconds). Time measured since ad is closed
		/// </summary>
		/// <value>The ads frequency.</value>
		int AdsFrequency { get; set; }

		/// <summary>
		/// Shows the ad.
		/// </summary>
		/// <param name="callback">Callback that will be called on ad close.</param>
		void ShowAd (AdClosed callback);

	}

	/// <summary>
	/// Ad closed.
	/// </summary>
	public delegate void AdClosed (AdClosedEvent data);

	/// <summary>
	/// Class, containing useful information about closed Ad
	/// </summary>
	public class AdClosedEvent
	{
		/// <summary>
		/// Reason why ad was closed
		/// </summary>
		/// <value>The reason.</value>
		public Reason Reason { get ; internal set; }

		/// <summary>
		/// Gets string containing additional information
		/// </summary>
		/// <value>The message.</value>
		public string Message{ get ; internal set; }

		/// <summary>
		/// List of <see cref="JukkoUnityAndroidSdk.EventInfo"/> events, that happened on Ad lifecycle. Can be empty.
		/// </summary>
		/// <value>The events.</value>
		public List<EventInfo> Events{ get ; internal set; }

		internal AdClosedEvent ()
		{
		}
	}

	/// <summary>
	/// Class for holding event and it's timestamp
	/// </summary>
	public class EventInfo
	{
		/// <summary>
		/// Type of event
		/// </summary>
		/// <value>The ad event.</value>
		public AdEvent AdEvent{ get; internal set; }

		/// <summary>
		/// Timestamp of the event
		/// </summary>
		/// <value>The timestamp.</value>
		public DateTime Timestamp{ get; internal set; }

		internal EventInfo (AdEvent e, DateTime t)
		{
			AdEvent = e;
			Timestamp = t;
		}
	}

	/// <summary>
	/// enum, containing possible reasons why ad closed
	/// </summary>
	public enum Reason
	{
		/// <summary>
		/// Ad was closed by user
		/// </summary>
		ClosedByUser,

		/// <summary>
		/// Api server was unresponsive
		/// </summary>
		Timeout,

		/// <summary>
		/// Network connectivity problems
		/// </summary>
		NetworkConnectivity,

		/// <summary>
		/// Ad is showing too often
		/// </summary>
		FrequencyCapping,

		/// <summary>
		/// Unspecified error
		/// Look at <see cref="JukkoUnityAndroidSdk.AdClosedEvent.Message"/> for additional info
		/// </summary>
		Error

	}

	/// <summary>
	/// enum, containig possible ad events
	/// </summary>
	public enum AdEvent
	{
		/// <summary>
		/// Ad activity opened
		/// </summary>
		Launch,

		/// <summary>
		/// Ad was closed
		/// </summary>
		Close,

		/// <summary>
		/// Intro was shown to user
		/// </summary>
		IntroShown,

		/// <summary>
		/// Outro was shown to user
		/// </summary>
		OutroShown,

		/// <summary>
		/// Progress was shown to user
		/// </summary>
		ProgressShown,

		/// <summary>
		/// Ad was shown to user
		/// </summary>
		AdShown,
		/// <summary>
		/// User clicked on ad link
		/// </summary>
		AdUrlOpened
	}
}