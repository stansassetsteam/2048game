﻿#if UNITY_ANDROID

using System;
using UnityEngine;


namespace Jukko.Android
{
	/// <summary>
	/// Jukko Android SDK.
	/// </summary>
	public class JukkoAndroidSdk : JukkoSdkInterface
	{
		
		public  JukkoAndroidSdk ()
		{
		}

		public void Init (string apiKey)
		{
			//TODO check if object recreation affects on performance
			//TODO should we add `using(){}` wrapper?
			AndroidJavaClass unityClass = new AndroidJavaClass (Utils.UnityActivityClassName);
			AndroidJavaObject unityActivity = unityClass.GetStatic<AndroidJavaObject> ("currentActivity");
			Utils.SdkInstance.Call ("init", unityActivity, apiKey);
		}

		public bool DebugMode {
			get {				
				return Utils.SdkInstance.Call<bool> ("isDebugMode");
			}
			set {				
				Utils.SdkInstance.Call<AndroidJavaObject> ("setDebugMode", value);
			}
		}


		public int AdsFrequency { 
			get {			
				return Utils.SdkInstance.Call<int> ("getAdsFrequency");
			} 
			set {
				
				Utils.SdkInstance.Call<AndroidJavaObject> ("setAdsFrequency", value);
			}
		}

		public void ShowAd (AdClosed callback)
		{		
			Utils.SdkInstance.Call ("showAd", new AdClosedCallback (callback));
		}
	}
}

#endif
