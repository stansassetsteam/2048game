﻿#if UNITY_ANDROID

using System;
using System.Collections.Generic;
using UnityEngine;

namespace Jukko.Android
{

	public class AdClosedCallback : AndroidJavaProxy
	{
		private event AdClosed onAdClosed;

		internal AdClosedCallback (AdClosed x) : base (Utils.JukkoAndroidSdkInterfaceCallbackName)
		{
			onAdClosed = x;
		}

		void onClosed (AndroidJavaObject closedEvent)
		{
			AdClosedEvent adEvent = Utils.FromAndroidObject (closedEvent);
			if (onAdClosed != null) {					
				onAdClosed.Invoke (adEvent);
				onAdClosed = null;
			}
		}
	}
}

#endif
