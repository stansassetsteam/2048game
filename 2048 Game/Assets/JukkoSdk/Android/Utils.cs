﻿#if UNITY_ANDROID

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Jukko.Android
{

	internal class Utils
	{
		public const string JukkoAndroidSdkClassName = "com.jukko.sdk.JukkoSdk";
		public const string JukkoAndroidSdkInterfaceName = "com.jukko.sdk.JukkoSdkInterface";
		public const string JukkoAndroidSdkInterfaceCallbackName = "com.jukko.sdk.JukkoSdkInterface$AdCallback";
		public const string DateClassName = "java.util.Date";
		public const string UnityActivityClassName = "com.unity3d.player.UnityPlayer";

		internal static Reason StringToReason (string s)
		{
			switch (s) {
			case "CLOSED_BY_USER":
				return Reason.ClosedByUser;
			case "TIMEOUT":
				return Reason.Timeout;
			case "NETWORK_CONNECTIVITY":
				return Reason.NetworkConnectivity;
			case "FREQUENCY_CAPPING":
				return Reason.FrequencyCapping;
			case "ERROR":
				return Reason.Error;
			}
			return Reason.Error;
		}

		internal static AdEvent StringToAdEvent (string s)
		{
			switch (s) {
			case "LAUNCH":
				return AdEvent.Launch;
			case "CLOSE":
				return AdEvent.Close;
			case "INTRO_SHOWN":
				return AdEvent.IntroShown;
			case "OUTRO_SHOWN":
				return AdEvent.OutroShown;
			case "PROGRESS_SHOWN":
				return AdEvent.ProgressShown;
			case "AD_SHOWN":
				return AdEvent.AdShown;
			case "AD_URL_OPENED":
				return AdEvent.AdUrlOpened;
			}
			throw new KeyNotFoundException (string.Format ("AdEvent with name '{0}' does not exist", s));
		}


		internal static AdClosedEvent FromAndroidObject (AndroidJavaObject androidObject)
		{
			AndroidJavaObject reason = androidObject.Get<AndroidJavaObject> ("reason");
			string reasonName = reason.Call<string> ("name");
			List<EventInfo> eventList = new List<EventInfo> ();
			string messageString = androidObject.Get<string> ("message");

			AndroidJavaObject eventListObject = androidObject.Get<AndroidJavaObject> ("events");
			if (eventListObject != null) {				
				eventList = new List<EventInfo> ();
				int size = eventListObject.Call<int> ("size");
				for (int i = 0; i < size; i++) {
					AndroidJavaObject item = eventListObject.Call<AndroidJavaObject> ("get", i);
					AndroidJavaObject adEventObject = item.Get<AndroidJavaObject> ("adEvent");
					AndroidJavaObject timestampObject = item.Get<AndroidJavaObject> ("timestamp");
					string adEvent = adEventObject.Call<string> ("name");
					AdEvent ae;
					try {
						ae = StringToAdEvent (adEvent);
					} catch (KeyNotFoundException e) {
						Debug.Log (string.Format ("Error while parsing AdEvent: {0}", e.Message));
						continue;
					}
					long timestamp = timestampObject.Call<long> ("getTime");
					DateTime dateTime = new DateTime (1970, 1, 1, 0, 0, 0, DateTimeKind.Utc); //unix time
					dateTime = dateTime.AddMilliseconds (timestamp);
					EventInfo ei = new EventInfo (ae, dateTime);
					eventList.Add (ei);
				}
			}

			AdClosedEvent result = new AdClosedEvent ();
			result.Reason = StringToReason (reasonName);
			result.Message = messageString;
			result.Events = eventList;
			return result;
		}

		internal static AndroidJavaObject SdkInstance {
			get {
				AndroidJavaClass sdkClass = new AndroidJavaClass (Utils.JukkoAndroidSdkClassName);
				return sdkClass.CallStatic<AndroidJavaObject> ("getInstance");
			}
		}
	}
}

#endif
