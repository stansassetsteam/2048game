﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA.UltimateAds {

	[System.Serializable]
	public class WinVunglePlacement : Placement {

		public WinVunglePlacement(string id) {
			ID = id;
		}
	}
}