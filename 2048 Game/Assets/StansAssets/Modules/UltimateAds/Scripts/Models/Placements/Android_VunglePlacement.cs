﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA.UltimateAds {

	[System.Serializable]
	public class ANVunglePlacement : Placement {

		public ANVunglePlacement(string id) {
			ID = id;
		}
	}
}
