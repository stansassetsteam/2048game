﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA.UltimateAds {

	[System.Serializable]
	public class IOSVunglePlacement : Placement {

		public IOSVunglePlacement(string id) {
			ID = id;
		}
	}
}