﻿using UnityEngine;
using System.Collections;

public class PlayerData : Singletone<PlayerData> {
	private const string IS_FIRST_GAME_START = "IS_FIRST_GAME_START";
	private const string ADS_LOCKED = "ADS_LOCKED";
	private const string SOUND_STATE = "SOUND_STATE";
	private const string NEW_GAME_COUNTER = "NEW_GAME_COUNTER";
	private const string IS_ALREADY_RATED = "IS_ALREADY_RATED";

	public bool IsFirstGameStart {
		get {
			if (!PlayerPrefs.HasKey (IS_FIRST_GAME_START) ) {
				PlayerPrefs.SetInt (IS_FIRST_GAME_START, 1);

				return true;
			}
			return false;
		}
	}

	public bool IsRated {
		get {
			if (PlayerPrefs.HasKey (IS_ALREADY_RATED)) {
				return true;
			}

			return false;
		}
		set {
			PlayerPrefs.SetInt (IS_ALREADY_RATED, 1);
		}
	}

	public bool IsSound {
		get {
			if (PlayerPrefs.HasKey (SOUND_STATE) && (PlayerPrefs.GetInt (SOUND_STATE) == 0) ) {
				return false;
			}

			return true;
		}
		set {
			if (value) {
				PlayerPrefs.SetInt (SOUND_STATE, 1);
			} else {
				PlayerPrefs.SetInt (SOUND_STATE, 0);
			}
		}
	}

	public int NewGameCounter {
		get {
			if (PlayerPrefs.HasKey (NEW_GAME_COUNTER)) {
				return PlayerPrefs.GetInt (NEW_GAME_COUNTER);
			}

			return 1;
		}
		set {
			PlayerPrefs.SetInt (NEW_GAME_COUNTER, value);
		}
	}

	public bool AdsBLockedByPurchase{
		get {
			if (PlayerPrefs.HasKey (ADS_LOCKED) && (PlayerPrefs.GetInt (ADS_LOCKED) == 1)) {
				return true;
			}

			return false;
		}
		set {
			if (value) {
				PlayerPrefs.SetInt (ADS_LOCKED, 1);
			} else {
				PlayerPrefs.SetInt (ADS_LOCKED, 0);
			}
		}
	}
}
