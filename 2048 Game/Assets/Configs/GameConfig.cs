﻿using UnityEngine;
using System.Collections;

public class GameConfig  {

	public const string SCORES_LEADERBOARD_ID = "scores";
	public const string BTS_FB_PAGE = "https://www.facebook.com/BeeTheSwarm/";

	public const string SHARE_TITLE = "2048 - Bee The Swarm";
	public const string SHARE_TEXT = "I scored {0} points at 2048 Bee The Swarm, a puzzle game where you have fun and do good things for the world! http://www.beetheswarm.com/";
	public const string INVITE_TEXT = "2048 Bee The Swarm, a puzzle game where you have fun and do good things for the world! https://campaign.beetheswarm.com/feed";


	public const string PLAY_STORE_URL = "https://play.google.com/store/apps/details?id=com.beetheswarm.the2048game";

	public const float MoveAnimationTime = 0.2f;

	public static float AllowableMoveError = 0.5f;
	public static float AllowableClickError = Screen.width / 20f;


	public const string EDIOTR_PLAYER_ID = "editor_dev2";

	public const int FirstGoalPower = 11;
}
