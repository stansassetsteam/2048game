﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[InitializeOnLoad]
public class SampleMyDependencies : AssetPostprocessor {

    public static object svcSupport;

    private static readonly string PLAY_SERVICES_VERSION = "10.0.1";
    private static readonly string ANDROID_SUPPORT_VERSION = "24.0.0";

    static SampleMyDependencies() {
        RegisterDependencies();
    }

    public static void RegisterDependencies()
    {
        var playServicesSupport = Google.VersionHandler.FindClass("Google.JarResolver", "Google.JarResolver.PlayServicesSupport");

        if (playServicesSupport == null)
            return;

        svcSupport = svcSupport ?? Google.VersionHandler.InvokeStaticMethod(
        playServicesSupport, "CreateInstance",
        new object[] {
           "StansAssets",
           EditorPrefs.GetString("AndroidSdkRoot"),
           "ProjectSettings"
        });

        Google.VersionHandler.InvokeInstanceMethod(svcSupport, "DependOn",
           new object[] {
            "com.google.android.gms",
            "play-services-games",
            PLAY_SERVICES_VERSION
           },
           namedArgs: new Dictionary<string, object>() {
             {"packageIds", new string[] { "extra-google-m2repository" } }
           });

        Google.VersionHandler.InvokeInstanceMethod(svcSupport, "DependOn",
           new object[] {
            "com.google.android.gms",
            "play-services-ads",
            PLAY_SERVICES_VERSION
           },
           namedArgs: new Dictionary<string, object>() {
                     {"packageIds", new string[] { "extra-google-m2repository" } }
           });

       Google.VersionHandler.InvokeInstanceMethod(svcSupport, "DependOn",
           new object[] {
            "com.google.android.gms",
            "play-services-auth",
            PLAY_SERVICES_VERSION
           },
           namedArgs: new Dictionary<string, object>() {
             {"packageIds", new string[] { "extra-google-m2repository" } }
           });

        Google.VersionHandler.InvokeInstanceMethod(svcSupport, "DependOn",
          new object[] {
            "com.google.android.gms",
            "play-services-analytics",
            PLAY_SERVICES_VERSION
          },
          namedArgs: new Dictionary<string, object>() {
             {"packageIds", new string[] { "extra-google-m2repository" } }
          });

        Google.VersionHandler.InvokeInstanceMethod(svcSupport, "DependOn",
          new object[] {
            "com.google.android.gms",
            "play-services-drive",
            PLAY_SERVICES_VERSION
          },
          namedArgs: new Dictionary<string, object>() {
             {"packageIds", new string[] { "extra-google-m2repository" } }
          });


        Google.VersionHandler.InvokeInstanceMethod(svcSupport, "DependOn",
           new object[] {
                    "com.google.android.gms",
                    "play-services-tasks",
                    PLAY_SERVICES_VERSION
           },
           namedArgs: new Dictionary<string, object>() {
                     {"packageIds", new string[] { "extra-google-m2repository" } }
           });

        
        Google.VersionHandler.InvokeInstanceMethod(svcSupport, "DependOn",
           new object[] {
                            "com.google.android.gms",
                            "play-services-iid",
                            PLAY_SERVICES_VERSION
           },
           namedArgs: new Dictionary<string, object>() {
                             {"packageIds", new string[] { "extra-google-m2repository" } }
           });

        Google.VersionHandler.InvokeInstanceMethod(svcSupport, "DependOn",
           new object[] {
                                    "com.google.android.gms",
                                    "play-services-plus",
                                    PLAY_SERVICES_VERSION
           },
           namedArgs: new Dictionary<string, object>() {
                                     {"packageIds", new string[] { "extra-google-m2repository" } }
           });

        Google.VersionHandler.InvokeInstanceMethod(svcSupport, "DependOn",
           new object[] {
                                    "com.google.android.gms",
                                    "play-services-gcm",
                                    PLAY_SERVICES_VERSION
           },
           namedArgs: new Dictionary<string, object>() {
                                     {"packageIds", new string[] { "extra-google-m2repository" } }
           });

        Google.VersionHandler.InvokeInstanceMethod(svcSupport, "DependOn",
   new object[] {
                                    "com.google.android.gms",
                                    "play-services-appinvite",
                                    PLAY_SERVICES_VERSION
   },
   namedArgs: new Dictionary<string, object>() {
                                     {"packageIds", new string[] { "extra-google-m2repository" } }
   });

        Google.VersionHandler.InvokeInstanceMethod(
           svcSupport, "DependOn",
           new object[] {
            "com.android.support",
            "support-v4",
            ANDROID_SUPPORT_VERSION
           },
           namedArgs: new Dictionary<string, object>() {
             {"packageIds", new string[] { "extra-android-m2repository" } }
           });
    }
}
